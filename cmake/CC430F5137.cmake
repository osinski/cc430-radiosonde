set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR msp430)

set(MCU "cc430f5137")

set(MCPU_FLAGS "-mmcu=${MCU}")
set(LD_FLAGS "-T ${MCU}.ld")

include(${CMAKE_CURRENT_LIST_DIR}/gcc-msp430.cmake)
