/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Serial Communication handling
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#include <cc430f5137.h>

#include "printf_with_colors.h"

#include "gpio.h"

#include "hal_usci_common.h"
#include "hal_usci_i2c.h"
#include "hal_usci_spi.h"
#include "hal_usci_uart.h"


void HAL_configureSerialComm(void);

void HAL_serial_spi_send(uint8_t *sendBuf, size_t sendSize);
void HAL_serial_spi_read(uint8_t *recvBuf, size_t readSize);
void HAL_serial_spi_sendAndRead(uint8_t *sendBuf, uint8_t *recvBuf,
                                size_t bufSize);
