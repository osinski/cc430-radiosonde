#include "serial_comm.h"


extern usci_clock_handler_t usci_ab_clockSettings;
extern usci_uart_handler_t usci_a_uart_handler;
extern usci_spi_handler_t usci_ab_spi_handler;

extern gpio_handler_t gpio_gpsCS;

void HAL_configureSerialComm(void)
{
    // SMCLK is 1MHz, divided by 15 gives ~67kHz. Max for UBLOX is 100kHz
    usci_ab_clockSettings.clock = USCI_CLOCK_SMCLK;
    usci_ab_clockSettings.baudrate_prescalers.baudrate = 255;
    usci_clock_init(USCI_B_0, &usci_ab_clockSettings);

    usci_ab_spi_handler.spiMode = USCI_SPI_MODE_MASTER;
    usci_ab_spi_handler.peripheral = USCI_B_0;
    usci_ab_spi_handler.csPinMode = USCI_SPI_CS_SOFTWARE;
    usci_ab_spi_handler.dataDirection = USCI_UART_SPI_DATADIRECTION_MSBFIRST;
    usci_ab_spi_handler.characterLen = USCI_UART_SPI_CHARLEN_8BIT;
    usci_ab_spi_handler.clkPhase = USCI_SPI_CLOCK_PHASE_CHANGE_CAPTURE;
    usci_ab_spi_handler.clkPolarity = USCI_SPI_CLOCK_POLARITY_IDLELOW;
    usci_spi_init(&usci_ab_spi_handler);

    usci_clearResetState(USCI_B_0);


    // standard 9600 baudrate for UART, values taken from User's Guide
    usci_ab_clockSettings.baudrate_prescalers.baudrate = 104;
    usci_clock_init(USCI_A_0, &usci_ab_clockSettings);
    usci_uart_setModulationStages(0, 1);

    usci_uart_init(&usci_a_uart_handler);

    usci_clearResetState(USCI_A_0);

    usci_enableInterrupt(USCI_A_0, USCI_INTERRUPTS_RX);
}

void HAL_serial_spi_send(uint8_t *sendBuf, size_t sendSize)
{
    gpio_write(&gpio_gpsCS, GPIO_LOW);

    for(uint8_t i = 0; i < sendSize; i++) {
        usci_spi_transmitByte_blocking(&usci_ab_spi_handler, sendBuf[i]);
    }

    gpio_write(&gpio_gpsCS, GPIO_HIGH);
}

void HAL_serial_spi_read(uint8_t *recvBuf, size_t readSize)
{
    uint8_t dummyByte = 0xff;

    gpio_write(&gpio_gpsCS, GPIO_LOW);

    for(uint8_t i = 0; i < readSize; i++) {
        usci_spi_transmitByte_blocking(&usci_ab_spi_handler, dummyByte);
        recvBuf[i] = usci_spi_receiveByte_blocking(&usci_ab_spi_handler);
    }

    gpio_write(&gpio_gpsCS, GPIO_HIGH);
}

void HAL_serial_spi_sendAndRead(uint8_t *sendBuf, uint8_t *recvBuf,
                                size_t bufSize)
{
    gpio_write(&gpio_gpsCS, GPIO_LOW);

    for(uint8_t i = 0; i < bufSize; i++) {
        usci_spi_transmitByte_blocking(&usci_ab_spi_handler, sendBuf[i]);
        recvBuf[i] = usci_spi_receiveByte_blocking(&usci_ab_spi_handler);
    }

    gpio_write(&gpio_gpsCS, GPIO_HIGH);
}


// for printf
void _putchar(char character)
{
    usci_uart_transmitByte_blocking((uint8_t) character);
}
