/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Radio Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef RADIO_H
#define RADIO_H


#include "hal_rf1a.h"

void HAL_configureRadio(void);


#endif //RADIO_H
