/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Timer Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef TIMER_H
#define TIMER_H

#include "stdbool.h"

#include "hal_timer_a.h"
#include "hal_rtc_a.h"
#include "system.h"

typedef struct
{
        volatile bool rttyToneDurationPeriodElapsed;
        volatile bool aprsToneTypePeriodElapsed;
        volatile bool aprsToneDurationPeriodElapsed;

} timerTimeoutFlags_t;

void HAL_initSystemTimer(void);
void HAL_initAuxillaryTimer(void);

void HAL_initRTC(void);
void HAL_haltRTC(void);
void HAL_triggerRTC(void);

#endif //TIMER_H
