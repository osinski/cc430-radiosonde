/*
 * radiosonde_config.h
 *
 *  Created on: 26.09.2018
 *      Author: dell
 */

#ifndef RADIOSONDE_CONFIG_H_
#define RADIOSONDE_CONFIG_H_

#define APRS_CARRIER_FREQ				437602000		// 432500000 Hz
#define RTTY_CARRIER_FREQ				437602000		// 434525000 Hz

#define APRS_DEST_ADDR					"GRND  1"	// both destination and source addresses are 7 bytes long
#define APRS_SRC_ADDR					"MOS   1"
#define APRS_DIGI_PATH					"WIDE1-1"

#define APRS_STARTFLAGS_NUMBER			10
#define APRS_ENDFLAGS_NUMBER			3

#define RTTY_CALLSIGN					"SP6ZWR"

#define RTTY_FREQ_DEVIATION				540.0f		//	540 Hz
#define RTTY_STARTBITS_NUMBER			100

#define RTTY_APRS_RATIO					3

#define TX_SEND_DELAY					10000	//	10000 ms	10s for RTTY, APRS_RTTY_RATIO x 10s for APRS


#endif /* RADIOSONDE_CONFIG_H_ */
