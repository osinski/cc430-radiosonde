/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Timer Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/


#include "timer.h"

timerTimeoutFlags_t timeoutFlags = { false, false, false };

tim_timerHandler_t timer_system;
tim_captureCompareHandler_t timer_systemCaptureCompare;

tim_timerHandler_t timer_auxillary;
tim_captureCompareHandler_t timer_radioSignalCaptureCompare;
tim_captureCompareHandler_t timer_auxillaryCaptureCompare;

extern controlFlags_t system_Ctrl;

void HAL_initSystemTimer(void)
{
    timer_system = tim_newDefaultTimer(TIM0);
    timer_system.clockSource = TIM_CLOCKSOURCE_ACLK;    // XT1 - 32768Hz
    timer_system.inputDivider = TIM_INPUTDIVIDER_8;
//    timer_system.inputDividerEx = TIM_INPUTDIVIDER_EX_4; // using dividerEx causes resets - TODO fix it
    timer_system.modeControl = TIM_MODE_UP;
    tim_setTimConfig(&timer_system);

    timer_systemCaptureCompare = tim_newDefaultCaptureCompare(TIM0, CC0);
//    timer_systemCaptureCompare.outputMode = TIM_CC_OUTPUTMODE_TOGGLE;
    tim_setCaptureCompareConfig(&timer_systemCaptureCompare);

    tim_setCaptureCompareReg(&timer_systemCaptureCompare, 40000);   // ~10s

    tim_enableCaptureCompareInterrupts(&timer_systemCaptureCompare);

//    tim_enableMainInterrupts(&timer_system);

    tim_clearTimer(&timer_system);
}

void HAL_initAuxillaryTimer(void)
{
    timer_auxillary = tim_newDefaultTimer(TIM1);
    timer_auxillary.clockSource = TIM_CLOCKSOURCE_ACLK;
    timer_auxillary.modeControl = TIM_MODE_UP;
    tim_setTimConfig(&timer_auxillary);

    timer_radioSignalCaptureCompare = tim_newDefaultCaptureCompare(TIM1, CC0);
    timer_radioSignalCaptureCompare.outputMode = TIM_CC_OUTPUTMODE_OUTBIT;
    tim_setCaptureCompareReg(&timer_radioSignalCaptureCompare, 640);    // calculated 655

    timer_auxillaryCaptureCompare = tim_newDefaultCaptureCompare(TIM1, CC1);
    timer_auxillaryCaptureCompare.outputMode = TIM_CC_OUTPUTMODE_TOGGLE_RESET;
//    tim_setCaptureCompareConfig(&timer_auxillaryCaptureCompare);
//    tim_setCaptureCompareReg(&timer_auxillaryCaptureCompare, 500);

    tim_enableCaptureCompareInterrupts(&timer_radioSignalCaptureCompare);

    tim_clearTimer(&timer_auxillary);
}

void HAL_initRTC(void)
{
    // get 1s timebase for the rest of the RTC
    rtc_a_initPrescaler(RTC_PRESCALER_0, RTC_CLOCK_SOURCE_ACLK,
                        RTC_PRESCALER_DIVIDER_256);
    rtc_a_initPrescaler(RTC_PRESCALER_1, RTC_CLOCK_SOURCE_PRESCALER,
                        RTC_PRESCALER_DIVIDER_128);

    rtc_a_initCounterMode(RTC_CLOCK_SOURCE_PRESCALER);
}

void HAL_triggerRTC(void)
{
    rtc_a_writeCounterReg(RTC_COUNTER_1_SECONDS,
                          0xff - system_Ctrl.sleepIntervalSeconds);

    rtc_a_clearInterruptFlag(RTC_INTERRUPT_TIME_EVENT);
    rtc_a_enableInterrupt(RTC_INTERRUPT_TIME_EVENT);

    rtc_a_clearHold_prescaler(RTC_PRESCALER_0);
    rtc_a_clearHold_prescaler(RTC_PRESCALER_1);
    rtc_a_clearHold_mainCounter();
}

void HAL_haltRTC(void)
{
    rtc_a_setHold_prescaler(RTC_PRESCALER_0);
    rtc_a_setHold_prescaler(RTC_PRESCALER_1);
    rtc_a_setHold_mainCounter();

    rtc_a_disableInterrupt(RTC_INTERRUPT_TIME_EVENT);
}
