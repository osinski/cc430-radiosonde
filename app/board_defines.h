/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F5137 Board Specific Defines
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#define GPIO_LED_PORT       GPIO_PORT_2
#define GPIO_LED_PIN        GPIO_PIN_7

#define ACLK_TEST_PORT      GPIO_PORT_2
#define ACLK_TEST_PIN       GPIO_PIN_6

#define SMCLK_TEST_PORT     GPIO_PORT_3
#define SMCLK_TEST_PIN      GPIO_PIN_7

#define RFGDO0_TEST_PORT    GPIO_PORT_1
#define RFGDO0_TEST_PIN     GPIO_PIN_0

#define RFGDO1_TEST_PORT    GPIO_PORT_3
#define RFGDO1_TEST_PIN     GPIO_PIN_6

#define RFGDO2_TEST_PORT    GPIO_PORT_1
#define RFGDO2_TEST_PIN     GPIO_PIN_1

#define GPS_MOSI_PORT       GPIO_PORT_1
#define GPS_MOSI_PIN        GPIO_PIN_6

#define GPS_MISO_PORT       GPIO_PORT_1
#define GPS_MISO_PIN        GPIO_PIN_5

#define GPS_SCK_PORT        GPIO_PORT_1
#define GPS_SCK_PIN         GPIO_PIN_7

#define GPS_CS_PORT         GPIO_PORT_1
#define GPS_CS_PIN          GPIO_PIN_4

#define GPS_EXTINT_PORT     GPIO_PORT_2
#define GPS_EXTINT_PIN      GPIO_PIN_0

#define DBG_UART_TX_PORT    GPIO_PORT_1
#define DBG_UART_TX_PIN     GPIO_PIN_6

#define DBG_UART_RX_PORT    GPIO_PORT_1
#define DBG_UART_RX_PIN     GPIO_PIN_5

#define SYS_I2C_SDA_PORT    GPIO_PORT_1
#define SYS_I2C_SDA_PIN     GPIO_PIN_3

#define SYS_I2C_SCL_PORT    GPIO_PORT_1
#define SYS_I2C_SCL_PIN     GPIO_PIN_2
