/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 System Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#include "system.h"


controlFlags_t system_Ctrl = {
    .totalPacketCount = 0,
    .tmpPacketCount = 0,

    .sleepIntervalSeconds = 10,

    .uartCmdsEnabled = true,
    .radioEnabled = false,
    .sendRTTY = true,
    .sendAPRS = false,
    .parseNMEA = true,

    .txEnabled = false,
    .txPending = false,
    .standby = false
};

uartCommandFlags_t system_UartCmds = {
    .command_1 = false,
    .command_2 = false,
    .command_3 = false,
    .command_4 = false,
    .command_5 = false,
};


extern pmm_svsm_handler_t pmm_config_svsm_highside;
extern pmm_svsm_handler_t pmm_config_svsm_lowside;

extern ucs_oscillatorSettings_t ucs_config_oscillatorSettings;
extern ucs_fllSettings_t ucs_config_fllSettings;


void HAL_configureSystemPower(void)
{
    pmm_setVCore(&pmm_config_svsm_highside, &pmm_config_svsm_lowside, PMM_VOLTAGE_LEVEL_MAX);
}

void HAL_configureClocks(void)
{
    // set GPIO for XT1
    gpio_setAsDefaultPeripheral_input(GPIO_PORT_5, GPIO_PIN_0);
    gpio_setAsDefaultPeripheral_input(GPIO_PORT_5, GPIO_PIN_1);

    ucs_setOscillatorConfiguration(&ucs_config_oscillatorSettings);

    /* Equation for DCO frequency (from User's Guide) is:
     *      f = D x (N+1) x (f_fllrefclk / n), where:
     *          - D is fll_feedbackDivider
     *          - N is fll_DCOmultiplier
     *          - f_fllrefclk/n is reference clock for fll divided by fll_referenceDivider
     *
     *      taking it into account, with settings as below we get freq ~16MHz
     */
    ucs_config_fllSettings.fll_referenceSource = UCS_FLL_REFERENCE_XT1CLK;
    ucs_config_fllSettings.fll_referenceDivider = UCS_FLL_REFCLOCK_DIVIDER_1;
    ucs_config_fllSettings.dco_frequencyRange = UCS_DCO_FREQRANGE_6000_23700_kHz;
    ucs_config_fllSettings.fll_feedbackDivider = UCS_CLK_DIVIDER_4;
    ucs_config_fllSettings.fll_DCOmultiplier = 121;
    ucs_setFllConfiguration(&ucs_config_fllSettings);

    ucs_clockSettings_t mclk;
    mclk = ucs_newDefaultClock(UCS_CLOCK_MCLK);
    mclk.clkSource = UCS_SOURCE_DCOCLK;
    mclk.clkSourceDivider = UCS_CLK_DIVIDER_1;
    ucs_clockEnable(&mclk);

    ucs_clockSettings_t smclk;
    smclk = ucs_newDefaultClock(UCS_CLOCK_SMCLK);
    smclk.clkSource = UCS_SOURCE_DCOCLK;
    smclk.clkSourceDivider = UCS_CLK_DIVIDER_16;
    ucs_clockEnable(&smclk);

    ucs_clockSettings_t aclk;
    aclk = ucs_newDefaultClock(UCS_CLOCK_ACLK);
    aclk.clkSource = UCS_SOURCE_XT1CLK;
    ucs_clockEnable(&aclk);
}
