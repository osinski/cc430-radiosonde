/*
 * radiosonde_control.h
 *
 *  Created on: 01.10.2018
 *      Author: dell
 */

#pragma once

#include "stdint.h"
#include "stdbool.h"

//	radiosonde sending control
typedef struct
{
	uint8_t packetSentCount;
	volatile bool txPending;
	volatile bool txEnabled;

	volatile bool radiosondeEnabled;

	volatile bool rttyPending;
	volatile bool aprsPending;

} radiosondeControlStruct;

radiosondeControlStruct radiosondeControl;

