/*
 * rtty.h
 *
 *  Created on: 25.09.2018
 *      Author: dell
 */

#pragma once

#include "stdbool.h"
#include "stdint.h"
//#include "stdio.h"

#include "nmeaParser.h"
#include "hal_rf1a.h"
#include "timer.h"
#include "radiosonde_config.h"

#define RTTY_MSG_LENGTH			200


void rtty_formatGpsStructToStr(nmea_gpsData_t *gpsStruct, char *rttyStr);
void rtty_sendBit(uint8_t offset);
void rtty_sendStartBits(uint8_t number);
void rtty_sendByte(uint8_t byte);
void rtty_sendPacket(nmea_gpsData_t *gpsStruct);

void rtty_setRadio(rf1a_settings_t *radioStruct);
