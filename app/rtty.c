/*
 * rtty.c
 *
 *  Created on: 25.09.2018
 *      Author: dell
 */

#include "rtty.h"


extern timerTimeoutFlags_t timeoutFlags;

extern rf1a_settings_t rfSettings_ooktest;
extern tim_timerHandler_t timer_auxillary;
extern tim_captureCompareHandler_t timer_radioSignalCaptureCompare;


static inline
void rtty_startTx(void)
{
    tim_OUTbit_clear(&timer_radioSignalCaptureCompare);
}

static inline
void rtty_stopTx(void)
{
    tim_OUTbit_set(&timer_radioSignalCaptureCompare);
}

static inline
void rtty_startTimer(void)
{
    tim_clearTimer(&timer_auxillary);
    tim_enableCaptureCompareInterrupts(&timer_radioSignalCaptureCompare);
}

static inline
void rtty_stopTimer(void)
{
//    tim_haltTimer(&timer_auxillary);
    tim_disableCaptureCompareInterrupts(&timer_radioSignalCaptureCompare);
}

static inline
bool rtty_isToneDurationTimeElapsed(void)
{
//    return tim_isCaptureCompareInterruptPending(&timer_radioSignalCaptureCompare);
    return timeoutFlags.rttyToneDurationPeriodElapsed;
}


void rtty_sendPacket(nmea_gpsData_t *gpsStruct)
{
//	char rttyString[RTTY_MSG_LENGTH];
//    char* rttyString = "TEST RTTY";
    char* rttyString = "  \r\n RADIOSONDE RTTY TEST \r\n";

//	rtty_formatGpsStructToStr(gpsStruct, rttyString);

//	printf("rtty formatted string: %s \r\n", rttyString);

    rtty_sendStartBits(RTTY_STARTBITS_NUMBER);

    uint8_t i = 0;
    while(rttyString[i] != '\0')
    {
        rtty_sendByte(rttyString[i]);

        i++;
    }

}

//void rtty_formatGpsStructToStr(gps_dataStruct *gpsStruct, char *rttyStr)
//{
//    sprintf(rttyStr,
//        "%02d:%02d:%02d | %02d*%02d.%02d' %c | %03d*%02d.%02d' %c | %d m | %d deg | %d kph",
//        gpsStruct->currHours, gpsStruct->currMinutes, gpsStruct->currSeconds,
//        gpsStruct->latitude_degs, gpsStruct->latitude_mins, gpsStruct->latitude_hmins,
//        gpsStruct->NSindicator != 0 ? gpsStruct->NSindicator : '?',
//        gpsStruct->longitude_degs, gpsStruct->longitude_mins, gpsStruct->longitude_hmins,
//        gpsStruct->EWindicator != 0 ? gpsStruct->EWindicator : '?',
//        gpsStruct->altitude, gpsStruct->course, gpsStruct->speedKph);
//}

void rtty_sendStartBits(uint8_t number)
{
	for(uint8_t i = 0; i < number; i++)
	{
        rtty_sendBit(0);
	}
}

void rtty_sendByte(uint8_t byte)
{
    uint8_t offset = 0;
    // send '0' - start bit
    rtty_sendBit(0);

	for(uint8_t i = 0; i < 7; i++)
	{
        if (byte & 1) {
            // send '1'
            offset = 1;
        } else {
            // send '0'
            offset = 0;
        }

        rtty_sendBit(offset);

        byte >>= 1;
	}

    // send '1' - stop bit
    rtty_sendBit(1);
}

void rtty_sendBit(uint8_t offset)
{
//    rfm69_hopFrequency(RTTY_CARRIER_FREQ + RTTY_FREQ_DEVIATION, &radioStruct);
    rf1a_changeMachineState(RF1A_STATE_IDLE);
    rf1a_writeSingleReg(FREQ0, rfSettings_ooktest.freq0 + (offset & 1));
    __delay_cycles(500);
    rf1a_changeMachineState(RF1A_STATE_TX);

    rtty_startTimer();
    rtty_startTx();

    while(!rtty_isToneDurationTimeElapsed());

    rtty_stopTx();
    rf1a_changeMachineState(RF1A_STATE_IDLE);
    rtty_stopTimer();

    // clear ToneDurationTimeElapsed flag
    timeoutFlags.rttyToneDurationPeriodElapsed = false;
}

void rtty_setRadio(rf1a_settings_t *radioConfig)
{

}
