/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * Ublox NEO6m configuration handling
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#include <stdint.h>
#include <stdio.h>

#include "serial_comm.h"


void ublox_readData(void);

void ublox_enableNMEAonSPI(void);

void ublox_initialize(void);

