#include "gpio.h"


gpio_handler_t gpio_led;
gpio_handler_t gpio_test;
gpio_handler_t gpio_gpsExtint;
gpio_handler_t gpio_gpsCS;

void HAL_configureGPIO(void)
{
    pmap_unlockRegisters();
    pmap_enableRuntimeConfiguration();
    pmap_lockRegisters();

    gpio_led = gpio_newDefaultGPIO(GPIO_LED_PORT, GPIO_LED_PIN);
    gpio_led.dir = GPIO_OUTPUT;
    gpio_led.function = GPIO_FUNCTION_IO;
//    gpio_led.function = GPIO_FUNCTION_PERIPHERAL;

//    gpio_led.mapping = PMAP_USCI_B0_SPI_SLAVE_NSS;
//    gpio_led.mapping = PMAP_USCI_A0_UART_TX;
//    gpio_led.mapping = PMAP_CLOCK_OUTPUT_MCLK;
//    gpio_led.mapping = PMAP_TA1CCR0;
//    gpio_led.mapping = PMAP_TA1CCR1;
//    gpio_led.mapping = PMAP_RADIO_GDO1;
    gpio_configure(&gpio_led);

    gpio_test = gpio_newDefaultGPIO(GPIO_PORT_3, GPIO_PIN_6);
    gpio_test.dir = GPIO_OUTPUT;
    gpio_test.function = GPIO_FUNCTION_PERIPHERAL;
    gpio_test.mapping = PMAP_TA1CCR0;
//    gpio_test.mapping = PMAP_USCI_B0_CLOCK;
    gpio_configure(&gpio_test);

    gpio_gpsCS = gpio_newDefaultGPIO(GPS_CS_PORT, GPS_CS_PIN);
    gpio_gpsCS.dir = GPIO_OUTPUT;
    gpio_gpsCS.function = GPIO_FUNCTION_IO;
    gpio_configure(&gpio_gpsCS);
//    gpio_setLow(&gpio_gpsCS);

    gpio_gpsExtint = gpio_newDefaultGPIO(GPS_EXTINT_PORT, GPS_EXTINT_PIN);
    gpio_gpsExtint.dir = GPIO_OUTPUT;
    gpio_gpsExtint.function = GPIO_FUNCTION_IO;
    gpio_configure(&gpio_gpsExtint);
    gpio_setHigh(&gpio_gpsExtint);

    gpio_setAsDefaultPeripheral_output(ACLK_TEST_PORT, ACLK_TEST_PIN);
    gpio_setAsDefaultPeripheral_output(SMCLK_TEST_PORT, SMCLK_TEST_PIN);

//    gpio_setAsDefaultPeripheral_output(GPS_MOSI_PORT, GPS_MOSI_PIN);
//    gpio_setAsDefaultPeripheral_input(GPS_MISO_PORT, GPS_MISO_PIN);
//    gpio_setAsDefaultPeripheral_output(GPS_SCK_PORT, GPS_SCK_PIN);

//    gpio_setAsDefaultPeripheral_output(SYS_I2C_SCL_PORT, SYS_I2C_SCL_PIN);
//    gpio_setAsDefaultPeripheral_output(SYS_I2C_SDA_PORT, SYS_I2C_SDA_PIN);

    gpio_handler_t commGpio = gpio_newDefaultGPIO(GPIO_PORT_1, GPS_MOSI_PIN);
    commGpio.function = GPIO_FUNCTION_PERIPHERAL;
    commGpio.dir = GPIO_OUTPUT;

    commGpio.mapping = PMAP_USCI_B0_SPI_MOSI;
    gpio_configure(&commGpio);

    commGpio.pin = GPS_MISO_PIN;
    commGpio.mapping = PMAP_USCI_B0_SPI_MISO;
    gpio_configure(&commGpio);

    commGpio.pin = GPS_SCK_PIN;
    commGpio.mapping = PMAP_USCI_B0_CLOCK;
//    commGpio.mapping = PMAP_TA1CCR0;
    gpio_configure(&commGpio);

//    commGpio.pin = GPS_CS_PIN;
//    commGpio.mapping = PMAP_USCI_B0_SPI_SLAVE_NSS;
//    gpio_configure(&commGpio);

    commGpio.pin = SYS_I2C_SCL_PIN;
    commGpio.mapping = PMAP_USCI_A0_UART_RX;
    gpio_configure(&commGpio);

    commGpio.pin = SYS_I2C_SDA_PIN;
    commGpio.mapping = PMAP_USCI_A0_UART_TX;
    gpio_configure(&commGpio);

    commGpio.port = GPIO_PORT_2;
    commGpio.pin = GPIO_PIN_1;
    commGpio.mapping = PMAP_USCI_B0_CLOCK;
    gpio_configure(&commGpio);

//    commGpio.pin = GPIO_PIN_2;
//    commGpio.mapping = PMAP_USCI_B0_SPI_MOSI;
//    gpio_configure(&commGpio);

//    commGpio.pin = GPIO_PIN_3;
//    commGpio.mapping = PMAP_USCI_B0_SPI_MISO;
//    gpio_configure(&commGpio);
}
