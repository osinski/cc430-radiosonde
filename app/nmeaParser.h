/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * NMEA Messages Parser
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef NMEAPARSER_H
#define NMEAPARSER_H

#include "string.h"
#include "stdbool.h"
#include "stdlib.h"
#include "printf_with_colors.h"
#include "stdint.h"


#define NMEA_LINE_MAX_LENGTH		82+1

/////////////					NOTE				////////////////
////	ALL TIMES ARE GIVEN IN UTC! WARSAW TIME ZONE IS UTC+2	////

/////////////	USEFUL NMEA MESSAGE "TAGS"	/////////////
#define GBS		"GBS"		//	GNSS Satellite Fault Detection
#define	GGA		"GGA"		//	Global Positioning Fix Data
#define GLL		"GLL"		//	Geographic Position - Latitude, Longitude
#define GSA		"GSA"		//	GNSS DOP and Active Satellites
#define RMC		"RMC"		//	Recommended Minimum Specific GNSS Data
#define VTG		"VTG"		//	Course over Ground and Ground Speed
#define ZDA		"ZDA"		//	Time and Date




////////////	NMEA MSG STRUCTS	///////////////////
// TODO check how to minimize size of these structs

typedef struct
{
//	char rawGBS[NMEA_LINE_MAX_LENGTH];
	char rawGGA[NMEA_LINE_MAX_LENGTH];
	char rawGLL[NMEA_LINE_MAX_LENGTH];
//	char rawGSA[NMEA_LINE_MAX_LENGTH];
	char rawRMC[NMEA_LINE_MAX_LENGTH];
	char rawVTG[NMEA_LINE_MAX_LENGTH];
//	char rawZDA[NMEA_LINE_MAX_LENGTH];

//	bool foundRawGBS;
	bool foundRawGGA;
	bool foundRawGLL;
//	bool foundRawGSA;
	bool foundRawRMC;
	bool foundRawVTG;
//	bool foundRawZDA;

} nmea_rawMsgs_t;




#define ERRTIME_FIELD_NO	1
#define ERRLAT_FIELD_NO		2
#define ERRLON_FIELD_NO		3
#define ERRALT_FIELD_NO		4

//typedef struct
//{
//	float 		errTime;			// time of error calculation (?) (RAIM test)	(NOTE! UTC)
//	float		errLat;				// expected error in latitude
//	float 		errLon;				// expected error in longitude
//	float		errAlt;				// expected error in altitude

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgGBS_t;						// SEE PAGE 67 of 'UBlox 6 Receiver Description' for more information




#define GGA_CURRTIME_FIELD_NO	1
#define GGA_LAT_FIELD_NO		2
#define GGA_NS_FIELD_NO			3
#define GGA_LON_FIELD_NO		4
#define GGA_EW_FIELD_NO			5
#define FS_FIELD_NO				6
#define NOSV_FIELD_NO			7
#define HDOP_FIELD_NO			8
#define MSALT_FIELD_NO			9

//typedef struct
//{

//	float		currTime;			// current time		(NOTE! UTC)
//	float 		lat;				// latitude
//	char 		NS;					// hemisphere indicator
//	float 		lon;				// longitude
//	char 		EW;				// hemisphere indicator
//	uint8_t		fs:3;				// position fix status indicator (can be 0, 1, 2 or 6
//	uint8_t 	noSV:4;				// number of satellites used
//	uint8_t 	hdop;				// horizontal dilution of precision
//	uint16_t	mslAlt;				// mean see level altitude

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgGGA_t;						// SEE PAGE 68 of 'UBlox 6 Receiver Description' for more information

typedef struct
{
    char currTimeUTC[7];
    char latitude[10];
    char ns;
    char longitude[11];
    char ew;
    char fixStatus;
    char mslAltitude[6];

    bool updated;

} nmea_msgGGA_t;




#define	GLL_LAT_FIELD_NO			1
#define GLL_NS_FIELD_NO				2
#define GLL_LON_FIELD_NO			3
#define GLL_EW_FIELD_NO				4
#define GLL_CURRTIME_FIELD_NO		5
#define GLL_STATUS_FIELD_NO			6

//typedef struct
//{
//	float		lat;				// latitude
//	char 		NS;					// hemisphere indicator
//	float		lon;				// longitude
//	char		EW;					// hemisphere indicator
//	float		currTime;			// current time		(NOTE! UTC)
//	char		status;				// status: V = data invalid or receiver warning, A = data valid

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgGLL_t;						// SEE PAGE 69 of 'UBlox 6 Receiver Description' for more information

typedef struct
{
    char currTimeUTC[7];
    char latitude[10];
    char ns;
    char longitude[11];
    char ew;
    char dataValidness;

    bool updated;

} nmea_msgGLL_t;



#define	SATNO1_FIELD_NO			3
#define SATNO2_FIELD_NO			4
#define SATNO3_FIELD_NO			5
#define SATNO4_FIELD_NO			6
#define SATNO5_FIELD_NO			7
#define SATNO6_FIELD_NO			8
#define SATNO7_FIELD_NO			9
#define SATNO8_FIELD_NO			10
#define SATNO9_FIELD_NO			11
#define SATNO10_FIELD_NO		12
#define SATNO11_FIELD_NO		13
#define SATNO12_FIELD_NO		14

//typedef struct
//{
//	uint8_t		satNo1:5;				// numbers of satellites used for navigation (only 12) (numbers in range from 1 to 32)
//	uint8_t		satNo2:5;
//	uint8_t		satNo3:5;
//	uint8_t		satNo4:5;
//	uint8_t		satNo5:5;
//	uint8_t		satNo6:5;
//	uint8_t		satNo7:5;
//	uint8_t		satNo8:5;
//	uint8_t		satNo9:5;
//	uint8_t		satNo10:5;
//	uint8_t		satNo11:5;
//	uint8_t		satNo12:5;

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgGSA_t;						// SEE PAGE 72 of 'UBlox 6 Receiver Description' for more information




#define POSFIXTIME_FIELD_NO		1
#define RMC_STATUS_FIELD_NO		2
#define RMC_LAT_FIELD_NO		3
#define RMC_NS_FIELD_NO			4
#define RMC_LON_FIELD_NO		5
#define RMC_EW_FIELD_NO			6
#define RMC_SPEED_FIELD_NO		7
#define RMC_COURSE_FIELD_NO		8
#define RMC_DATE_FIELD_NO       9

//typedef struct
//{
//	float		posFixTime;			// time of position fix		(NOTE! UTC)
//	char		status;				// status: V = navigation receiver warning, A = data valid
//	float		lat;				// latitude
//	char 		NS;					// hemisphere indicator
//	float		lon;				// longitude
//	char		EW;					// hemisphere
//	float		speed;				// speed over ground (knots)
//	float		course;				// course over ground (degrees)

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgRMC_t;						// SEE PAGE 75 of 'UBlox 6 Receiver Description' for more information

typedef struct
{
    char fixTimeUTC[7];
    char dataValidness;
    char latitude[10];
    char ns;
    char longitude[11];
    char ew;
    char speedKts[7];
    char courseOverGnd[7];
    char date[7];

    bool updated;

} nmea_msgRMC_t;



#define COURSE_FIELD_NO		1
#define SPEEDKT_FIELD_NO	5
#define SPEEDKPH_FIELD_NO	7

//typedef struct
//{
//	float		course;				// course over ground (degrees)
//	float		speedKt;			// speed over ground (knots)
//	float		speedKph;			// speed over ground (kilometers per hour)

//	// there's more info but it's not needed

//	bool		updated;

//} nmea_msgVTG_t;						// SEE PAGE 78 of 'UBlox 6 Receiver Description' for more information

typedef struct
{
    char courseOverGnd[7];
    char speedKts[7];
    char speedKph[7];

    bool updated;

} nmea_msgVTG_t;



//	shouldn't be needed - contains only time (could be acquired from other messages) and date
#define TIME_FIELD_NO		1
#define DAY_FIELD_NO		2
#define MONTH_FIELD_NO		3
#define YEAR_FIELD_NO		4

//typedef struct
//{
//	float		time;				// current time		(NOTE! UTC)
//	uint8_t		day:5;
//	uint8_t		month:4;
//	uint16_t	year;

//	bool		updated;

//} nmea_msgZDA_t;						// SEE PAGE 79 of 'UBlox 6 Receiver Description' for more information

//typedef struct
//{
//    char currTimeUTC[7];
//    char day[2];
//    char month[2];
//    char year[4];

//    bool updated;

//} nmea_msgZDA_t;

////////////	MSG STRUCTS END		///////////////////////

//typedef struct
//{
//	uint8_t 	currHours;
//	uint8_t 	currMinutes;
//	uint8_t 	currSeconds;
//	uint8_t 	fixHours;
//	uint8_t 	fixMinutes;
//	uint8_t 	fixSeconds;
//	uint8_t 	errHours;
//	uint8_t 	errMinutes;
//	uint8_t 	errSeconds;

//	char		EWindicator;
//	char		NSindicator;

//	uint8_t 	svNo;			// number of satellites visible by GPS receiver
//	uint8_t 	latitude_degs;
//	uint8_t		latitude_mins;
//	uint8_t		latitude_hmins;
//	uint8_t 	errLat;
//	uint8_t		longitude_degs;
//	uint8_t		longitude_mins;
//	uint8_t		longitude_hmins;
//	uint8_t		errLon;
//	uint8_t		errAlt;
//	uint8_t		speedKt;		// speed in knots
//	uint8_t		speedKph;		// speed in kph
//	uint16_t	course;			// course over ground
//	uint16_t	altitude;

//} nmea_gpsData_t;

typedef struct
{
    char currTimeUTC[7];
    char fixTimeUTC[7];
    char date[7];

    char latitude[10];
    char ns;
    char longitude[11];
    char ew;
    char fixStatus;
    char mslAltitude[6];
    char dataValidness;

    char speedKts[7];
    char speedKph[7];
    char courseOverGnd[7];

} nmea_gpsData_t;


////////////////	FUNCTIONS	///////////////////
//bool nmea_checkForGBS(char *msgToCheck);
bool nmea_checkForGGA(char *msgToCheck);
bool nmea_checkForGLL(char *msgToCheck);
//bool nmea_checkForGSA(char *msgToCheck);
bool nmea_checkForRMC(char *msgToCheck);
bool nmea_checkForVTG(char *msgToCheck);
bool nmea_checkForZDA(char *msgToCheck);

void nmea_cutBufToRawMsgs(char *bufToCheck, nmea_rawMsgs_t  *rawMsgs);

//void nmea_formatStructGBS(char *rawGBS, nmea_msgGBS_t *msgGBS);
void nmea_formatStructGGA(char *rawGGA, nmea_msgGGA_t *msgGGA);
void nmea_formatStructGLL(char *rawGLL, nmea_msgGLL_t *msgGLL);
//void nmea_formatStructGSA(char *rawGSA, nmea_msgGSA_t *msgGSA);
void nmea_formatStructRMC(char *rawRMC, nmea_msgRMC_t *msgRMC);
void nmea_formatStructVTG(char *rawVTG, nmea_msgVTG_t *msgVTG);
//void nmea_formatStructZDA(char *rawZDA, nmea_msgZDA_t *msgZDA);

//void nmea_formatFinalGpsStruct(nmea_msgGBS_t *msgGBS, nmea_msgGGA_t *msgGGA,
//                               nmea_msgGLL_t *msgGLL, nmea_msgGSA_t *msgGSA,
//                               nmea_msgRMC_t *msgRMC, nmea_msgVTG_t *msgVTG,
//                               nmea_msgZDA_t *msgZDA, nmea_gpsData_t *finalGpsData);
void nmea_formatFinalGpsStruct(nmea_msgGGA_t *msgGGA, nmea_msgGLL_t *msgGLL,
                               nmea_msgRMC_t *msgRMC, nmea_msgVTG_t *msgVTG,
                               nmea_gpsData_t *finalGpsData);

void nmea_formatFinalGpsStruct();

void nmea_parse(char *gpsDataBuf, nmea_gpsData_t *finalGpsData);

uint8_t nmea_getChecksum(char* nmeaMsg);
uint8_t nmea_countChecksum(char* nmeaMsg);

#endif /* NMEAPARSER_H */
