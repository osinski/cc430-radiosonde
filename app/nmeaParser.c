/*
 * NEO6.c
 *
 *  Created on: 18.08.2018
 *      Author: dell
 */

#include "nmeaParser.h"

static bool nmeaConvPending;

void nmea_parse(char *gpsDataBuf, nmea_gpsData_t *finalGpsData)
{
    nmea_rawMsgs_t rawMsgs;
//    rawMsgs.foundRawGBS = false;
    rawMsgs.foundRawGGA = false;
    rawMsgs.foundRawGLL = false;
//    rawMsgs.foundRawGSA = false;
    rawMsgs.foundRawRMC = false;
    rawMsgs.foundRawVTG = false;
//    rawMsgs.foundRawZDA = false;

//    nmea_msgGBS_t msgGBS;	msgGBS.updated = false;
    nmea_msgGGA_t msgGGA;	msgGGA.updated = false;
    nmea_msgGLL_t msgGLL;	msgGLL.updated = false;
//    nmea_msgGSA_t msgGSA;	msgGSA.updated = false;
    nmea_msgRMC_t msgRMC;	msgRMC.updated = false;
    nmea_msgVTG_t msgVTG;	msgVTG.updated = false;
//    nmea_msgZDA_t msgZDA;	msgZDA.updated = false;

    printf("Parsing NMEA buffer\r\n");

    nmea_cutBufToRawMsgs(gpsDataBuf, &rawMsgs);

//    if(rawMsgs.foundRawGBS) {
//        printf(" GBS: &s \r\n", rawMsgs.rawGBS);
//        nmea_formatStructGBS(rawMsgs.rawGBS, &msgGBS);
//    }

    if(rawMsgs.foundRawGGA) {
        printf(" GGA: %s \r\n", rawMsgs.rawGGA);
        nmea_formatStructGGA(rawMsgs.rawGGA, &msgGGA);
    }

    if(rawMsgs.foundRawGLL) {
        printf(" GLL: %s \r\n", rawMsgs.rawGLL);
        nmea_formatStructGLL(rawMsgs.rawGLL, &msgGLL);
    }

//    if(rawMsgs.foundRawGSA) {
//        printf(" GSA: %s \r\n", rawMsgs.rawGSA);
//        nmea_formatStructGSA(rawMsgs.rawGSA, &msgGSA);
//    }

    if(rawMsgs.foundRawRMC) {
        printf(" RMC: %s \r\n", rawMsgs.rawRMC);
        nmea_formatStructRMC(rawMsgs.rawRMC, &msgRMC);
    }

    if(rawMsgs.foundRawVTG) {
        printf(" VTG: %s \r\n", rawMsgs.rawVTG);
        nmea_formatStructVTG(rawMsgs.rawVTG, &msgVTG);
    }

//    if(rawMsgs.foundRawZDA) {
//        printf(" GBS: &s \r\n", rawMsgs.rawZDA);
//        nmea_formatStructZDA(rawMsgs.rawZDA, &msgZDA);
//    }


    nmea_formatFinalGpsStruct(&msgGGA, &msgGLL, &msgRMC, &msgVTG, finalGpsData);

    printf(ANSI_CODE_GREEN); printf(ANSI_CODE_BLINK);
    printf("PARSED GPS DATA:\r\n");
    printf(ANSI_CODE_RESET);
    printf("    currTimeUTC:    %s\r\n\
    fixTimeUTC:     %s\r\n\
    date:           %s\r\n\
    latitude:       %s\r\n\
    longitude:      %s\r\n\
    mslAltitude:    %s\r\n",
           finalGpsData->currTimeUTC, finalGpsData->fixTimeUTC,
           finalGpsData->date, finalGpsData->latitude, finalGpsData->longitude,
           finalGpsData->mslAltitude);

}

void nmea_cutBufToRawMsgs(char *bufToCut, nmea_rawMsgs_t *rawMsgs)
{
    nmeaConvPending = true;

    char *line, *cleanedBuf;

    // check if buf contains any $ signs (which should indicate that there is at
    // lease one proper message)
    if((cleanedBuf = strchr(bufToCut, '$')) != NULL)
    {
        // cut into lines
        while((line = strsep(&cleanedBuf, "\n")) != NULL)
        {
//            if(nmea_checkForGBS(line))
//            {
//                strcpy(rawMsgs->rawGBS, line);
//                rawMsgs->foundRawGBS = true;
//            }

            if(nmea_checkForGGA(line))
            {
                strcpy(rawMsgs->rawGGA, line);
                rawMsgs->foundRawGGA = true;
            }

            if(nmea_checkForGLL(line))
            {
                strcpy(rawMsgs->rawGLL, line);
                rawMsgs->foundRawGLL = true;
            }

//            if(nmea_checkForGSA(line))
//            {
//                strcpy(rawMsgs->rawGSA, line);
//                rawMsgs->foundRawGSA = true;
//            }

            if(nmea_checkForRMC(line))
            {
                strcpy(rawMsgs->rawRMC, line);
                rawMsgs->foundRawRMC = true;
            }

            if(nmea_checkForVTG(line))
            {
                strcpy(rawMsgs->rawVTG, line);
                rawMsgs->foundRawVTG = true;
            }
        }
    }

    nmeaConvPending = false;
}

//void nmea_formatStructGBS(char *rawGBS, nmea_msgGBS_t *msgGBS)
//{
//    uint8_t tokenCounter = 0;

//    uint8_t readedChecksum = nmea_getChecksum(rawGBS);
//    uint8_t countedChecksum = nmea_countChecksum(rawGBS);

//    if(readedChecksum == countedChecksum)
//    {
//        char *token;

//        while((token = strsep(&rawGBS, ",")) != NULL)
//        {
//            if(tokenCounter == ERRTIME_FIELD_NO)
////				msgGBS->errTime = strtof(token, NULL);

//            if(tokenCounter == ERRLAT_FIELD_NO)
////				msgGBS->errLat = strtof(token, NULL);

//            if(tokenCounter == ERRLON_FIELD_NO)
////				msgGBS->errLon = strtof(token, NULL);

//            if(tokenCounter == ERRALT_FIELD_NO)
////				msgGBS->errAlt = strtof(token, NULL);

//            tokenCounter++;
//        }

//        msgGBS->updated = true;
//    }
//}

void nmea_formatStructGGA(char *rawGGA, nmea_msgGGA_t *msgGGA)
{
    uint8_t tokenCounter = 0;

    uint8_t readedChecksum = nmea_getChecksum(rawGGA);
    uint8_t countedChecksum = nmea_countChecksum(rawGGA);

    if(readedChecksum == countedChecksum)
    {
        char *token;

        while((token = strsep(&rawGGA, ",")) != NULL)
        {
            if(tokenCounter == GGA_CURRTIME_FIELD_NO)
                strncpy(msgGGA->currTimeUTC, token, sizeof(msgGGA->currTimeUTC)-1);

            if(tokenCounter == GGA_LAT_FIELD_NO)
                strncpy(msgGGA->latitude, token, sizeof(msgGGA->latitude)-1);

            if(tokenCounter == GGA_NS_FIELD_NO)
                msgGGA->ns = *token;

            if(tokenCounter == GGA_LON_FIELD_NO)
                strncpy(msgGGA->longitude, token, sizeof(msgGGA->longitude)-1);

            if(tokenCounter == GGA_EW_FIELD_NO)
                msgGGA->ew = *token;

            if(tokenCounter == FS_FIELD_NO)
                msgGGA->fixStatus = *token;

            if(tokenCounter == MSALT_FIELD_NO)
                strncpy(msgGGA->mslAltitude, token, sizeof(msgGGA->mslAltitude)-1);

            tokenCounter++;
        }

        msgGGA->updated = true;
    }
}

void nmea_formatStructGLL(char *rawGLL, nmea_msgGLL_t *msgGLL)
{
    uint8_t tokenCounter = 0;

    uint8_t readedChecksum = nmea_getChecksum(rawGLL);
    uint8_t countedChecksum = nmea_countChecksum(rawGLL);

    if(readedChecksum == countedChecksum)
    {
        char *token;

        while((token = strsep(&rawGLL, ",")) != NULL)
        {
            if(tokenCounter == GLL_LAT_FIELD_NO)
                strncpy(msgGLL->latitude, token, sizeof(msgGLL->latitude)-1);

            if(tokenCounter == GLL_NS_FIELD_NO)
                msgGLL->ns = *token;

            if(tokenCounter == GLL_LON_FIELD_NO)
                strncpy(msgGLL->longitude, token, sizeof(msgGLL->longitude)-1);

            if(tokenCounter == GLL_EW_FIELD_NO)
                msgGLL->ew = *token;

            if(tokenCounter == GLL_CURRTIME_FIELD_NO)
                strncpy(msgGLL->currTimeUTC, token, sizeof(msgGLL->currTimeUTC)-1);

            if(tokenCounter == GLL_STATUS_FIELD_NO)
                msgGLL->dataValidness = *token;

            tokenCounter++;
        }

        msgGLL->updated = true;
    }
}

//void nmea_formatStructGSA(char *rawGSA, nmea_msgGSA_t *msgGSA)
//{
//    uint8_t tokenCounter = 0;

//    uint8_t readedChecksum = nmea_getChecksum(rawGSA);
//    uint8_t countedChecksum = nmea_countChecksum(rawGSA);

//    if(readedChecksum == countedChecksum)
//    {
//        char *token;

//        while((token = strsep(&rawGSA, ",")) != NULL)
//        {
//            if(tokenCounter == SATNO1_FIELD_NO)
//                msgGSA->satNo1 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO2_FIELD_NO)
//                msgGSA->satNo2 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO3_FIELD_NO)
//                msgGSA->satNo3 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO4_FIELD_NO)
//                msgGSA->satNo4 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO5_FIELD_NO)
//                msgGSA->satNo5 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO6_FIELD_NO)
//                msgGSA->satNo6 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO7_FIELD_NO)
//                msgGSA->satNo7 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO8_FIELD_NO)
//                msgGSA->satNo8 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO9_FIELD_NO)
//                msgGSA->satNo9 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO10_FIELD_NO)
//                msgGSA->satNo10 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO11_FIELD_NO)
//                msgGSA->satNo11 = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == SATNO12_FIELD_NO)
//                msgGSA->satNo12 = (uint8_t) strtol(token, NULL, 10);

//            tokenCounter++;
//        }

//        msgGSA->updated = true;
//    }
//}

void nmea_formatStructRMC(char *rawRMC, nmea_msgRMC_t *msgRMC)
{
    uint8_t tokenCounter = 0;

    uint8_t readedChecksum = nmea_getChecksum(rawRMC);
    uint8_t countedChecksum = nmea_countChecksum(rawRMC);

    if(readedChecksum == countedChecksum)
    {
        char *token;

        while((token = strsep(&rawRMC, ",")) != NULL)
        {
            if(tokenCounter == POSFIXTIME_FIELD_NO)
                strncpy(msgRMC->fixTimeUTC, token, sizeof(msgRMC->fixTimeUTC)-1);

            if(tokenCounter == RMC_STATUS_FIELD_NO)
                msgRMC->dataValidness = *token;

            if(tokenCounter == RMC_LAT_FIELD_NO)
                strncpy(msgRMC->latitude, token, sizeof(msgRMC->latitude)-1);

            if(tokenCounter == RMC_NS_FIELD_NO)
                msgRMC->ns = *token;

            if(tokenCounter == RMC_LON_FIELD_NO)
                strncpy(msgRMC->longitude, token, sizeof(msgRMC->longitude)-1);

            if(tokenCounter == RMC_EW_FIELD_NO)
                msgRMC->ew = *token;

            if(tokenCounter == RMC_SPEED_FIELD_NO)
                strncpy(msgRMC->speedKts, token, sizeof(msgRMC->speedKts)-1);

            if(tokenCounter == RMC_COURSE_FIELD_NO)
                strncpy(msgRMC->courseOverGnd, token, sizeof(msgRMC->courseOverGnd)-1);

            if(tokenCounter == RMC_DATE_FIELD_NO)
                strncpy(msgRMC->date, token, sizeof(msgRMC->date)-1);

            tokenCounter++;
        }

        msgRMC->updated = true;
    }
}

void nmea_formatStructVTG(char *rawVTG, nmea_msgVTG_t *msgVTG)
{
    uint8_t tokenCounter = 0;

    uint8_t readedChecksum = nmea_getChecksum(rawVTG);
    uint8_t countedChecksum = nmea_countChecksum(rawVTG);

    if(readedChecksum == countedChecksum)
    {
        char *token;

        while((token = strsep(&rawVTG, ",")) != NULL)
        {
            if(tokenCounter == COURSE_FIELD_NO)
                strncpy(msgVTG->courseOverGnd, token, sizeof(msgVTG->courseOverGnd)-1);

            if(tokenCounter == SPEEDKT_FIELD_NO)
                strncpy(msgVTG->speedKts, token, sizeof(msgVTG->speedKts)-1);

            if(tokenCounter == SPEEDKPH_FIELD_NO)
                strncpy(msgVTG->speedKph, token, sizeof(msgVTG->speedKph)-1);

            tokenCounter++;
        }

        msgVTG->updated = true;
    }
}

//void nmea_formatStructZDA(char *rawZDA, nmea_msgZDA_t *msgZDA)
//{
//    uint8_t tokenCounter = 0;

//    uint8_t readedChecksum = nmea_getChecksum(rawZDA);
//    uint8_t countedChecksum = nmea_countChecksum(rawZDA);

//    if(readedChecksum == countedChecksum)
//    {
//        char *token;

//        while((token = strsep(&rawZDA, ",")) != NULL)
//        {
//            if(tokenCounter == TIME_FIELD_NO)
////				msgZDA->time = strtof(token, NULL);

//            if(tokenCounter == DAY_FIELD_NO)
//                msgZDA->day = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == MONTH_FIELD_NO)
//                msgZDA->month = (uint8_t) strtol(token, NULL, 10);

//            if(tokenCounter == YEAR_FIELD_NO)
//                msgZDA->year = (uint8_t) strtol(token, NULL, 10);

//            tokenCounter++;
//        }

//        msgZDA->updated = true;
//    }
//}

//void nmea_formatFinalGpsStruct(nmea_msgGBS_t *msgGBS, nmea_msgGGA_t *msgGGA,
//                               nmea_msgGLL_t *msgGLL, nmea_msgGSA_t *msgGSA,
//                               nmea_msgRMC_t *msgRMC, nmea_msgVTG_t *msgVTG,
//                               nmea_msgZDA_t *msgZDA,
//                               nmea_gpsData_t *finalGpsData)
//{
//    if(msgGBS->updated == true)
//    {
//        finalGpsData->errHours = (uint8_t)(msgGBS->errTime / 10000);
//        finalGpsData->errMinutes = msgGBS->errTime / 100 - finalGpsData->errHours * 100;
//        finalGpsData->errSeconds = msgGBS->errTime - (finalGpsData->errMinutes * 100 + finalGpsData->errHours * 10000);
//        finalGpsData->errLat = msgGBS->errLat;
//        finalGpsData->errLon = msgGBS->errLon;
//        finalGpsData->errAlt = msgGBS->errAlt;
//    }

//    if(msgGGA->updated == true)
//    {
//        if(msgGGA->fs != 0)	// fs - fix status (0-invalid, 1-2D/3D, 2-differential, 6-estimated(DR))
//        {
//            finalGpsData->currHours =  msgGGA->currTime / 10000;
//            finalGpsData->currMinutes = (msgGGA->currTime / 100) - (finalGpsData->currHours * 100);
//            finalGpsData->currSeconds = msgGGA->currTime - (finalGpsData->currMinutes * 100 + finalGpsData->currHours * 10000);
//            finalGpsData->latitude_degs = msgGGA->lat / 100;
//            finalGpsData->latitude_mins = msgGGA->lat - (finalGpsData->latitude_degs * 100);
//            finalGpsData->latitude_hmins = (msgGGA->lat - (uint16_t)msgGGA->lat) * 100;
//            finalGpsData->NSindicator = msgGGA->NS;
//            finalGpsData->longitude_degs = msgGGA->lon / 100;
//            finalGpsData->longitude_mins = msgGGA->lon - (finalGpsData->longitude_degs * 100);
//            finalGpsData->longitude_hmins = (msgGGA->lon - (uint16_t)msgGGA->lon) * 100;
//            finalGpsData->EWindicator = msgGGA->EW;
//            finalGpsData->svNo = msgGGA->noSV;
//            finalGpsData->altitude = msgGGA->mslAlt;
//        }
//    }

//    if(msgGLL->updated == true)
//    {
//        if(msgGLL->status == 'A')	// A - data vaild, V - data invalid
//        {
//            finalGpsData->latitude_degs = msgGLL->lat / 100;
//            finalGpsData->latitude_mins = msgGLL->lat - (finalGpsData->latitude_degs * 100);
//            finalGpsData->latitude_hmins = (msgGLL->lat - (uint16_t)msgGLL->lat) * 100;
//            finalGpsData->NSindicator = msgGLL->NS;
//            finalGpsData->longitude_degs = msgGLL->lon / 100;
//            finalGpsData->longitude_mins = msgGLL->lon - (finalGpsData->longitude_degs * 100);
//            finalGpsData->longitude_hmins = (msgGLL->lon - (uint16_t)msgGLL->lon) * 100;
//            finalGpsData->EWindicator = msgGLL->EW;
//            finalGpsData->currHours = msgGLL->currTime / 10000;
//            finalGpsData->currMinutes = (msgGLL->currTime / 100) - (finalGpsData->currHours * 100);
//            finalGpsData->currSeconds = msgGLL->currTime - (finalGpsData->currMinutes * 100 + finalGpsData->currHours * 10000);
//        }
//    }

//    if(msgGSA->updated == true)
//    {
//        // satellites used for navigation and operating mode - no fix / 2D fix / 3D fix
//    }

//    if(msgRMC->updated == true)
//    {
//        if(msgRMC->status == 'A')
//        {
//            finalGpsData->fixHours = msgRMC->posFixTime / 10000;
//            finalGpsData->fixMinutes = (msgRMC->posFixTime / 100) - (finalGpsData->fixHours * 100);
//            finalGpsData->fixSeconds = msgRMC->posFixTime - (finalGpsData->fixMinutes * 100 + finalGpsData->fixHours * 10000);
//            finalGpsData->latitude_degs = msgGGA->lat / 100;
//            finalGpsData->latitude_mins = msgGGA->lat - (finalGpsData->latitude_degs * 100);
//            finalGpsData->latitude_hmins = (msgGGA->lat - (uint16_t)msgGGA->lat) * 100;
//            finalGpsData->NSindicator = msgGGA->NS;
//            finalGpsData->longitude_degs = msgGGA->lon / 100;
//            finalGpsData->longitude_mins = msgGGA->lon - (finalGpsData->longitude_degs * 100);
//            finalGpsData->longitude_hmins = (msgGGA->lon - (uint16_t)msgGGA->lon) * 100;
//            finalGpsData->EWindicator = msgGGA->EW;
//            finalGpsData->speedKt = msgRMC->speed;
//            finalGpsData->course = msgRMC->course;
//        }
//    }

//    if(msgVTG->updated == true)
//    {
//        finalGpsData->course = msgVTG->course;
//        finalGpsData->speedKt = msgVTG->speedKt;
//        finalGpsData->speedKph = msgVTG->speedKph;
//    }

//    if(msgZDA->updated == true)
//    {
//        // time and date - not necessarily needed
//    }
//}

void nmea_formatFinalGpsStruct(nmea_msgGGA_t *msgGGA, nmea_msgGLL_t *msgGLL,
                               nmea_msgRMC_t *msgRMC, nmea_msgVTG_t *msgVTG,
                               nmea_gpsData_t *finalGpsData)
{
    if(msgGGA->updated == true)
    {
        if(msgGGA->fixStatus != '0')	// fs - fix status (0-invalid, 1-2D/3D, 2-differential, 6-estimated(DR))
        {
            strncpy(finalGpsData->currTimeUTC, msgGGA->currTimeUTC,
                    sizeof(finalGpsData->currTimeUTC)-1);
            strncpy(finalGpsData->latitude, msgGGA->latitude,
                    sizeof(finalGpsData->latitude)-1);
            finalGpsData->ns = msgGGA->ns;
            strncpy(finalGpsData->longitude, msgGGA->longitude,
                    sizeof(finalGpsData->longitude)-1);
            finalGpsData->ew = msgGGA->ew;
            strncpy(finalGpsData->mslAltitude, msgGGA->mslAltitude,
                    sizeof(finalGpsData->mslAltitude)-1);
        }
    }

    if(msgGLL->updated == true)
    {
        if(msgGLL->dataValidness == 'A')	// A - data vaild, V - data invalid
        {
            strncpy(finalGpsData->currTimeUTC, msgGLL->currTimeUTC,
                    sizeof(finalGpsData->currTimeUTC)-1);
            strncpy(finalGpsData->latitude, msgGLL->latitude,
                    sizeof(finalGpsData->latitude)-1);
            finalGpsData->ns = msgGLL->ns;
            strncpy(finalGpsData->longitude, msgGLL->longitude,
                    sizeof(finalGpsData->longitude)-1);
            finalGpsData->ew = msgGLL->ew;

        }
    }

    if(msgVTG->updated == true)
    {
        strncpy(finalGpsData->courseOverGnd, msgVTG->courseOverGnd,
                sizeof(finalGpsData->courseOverGnd)-1);
        strncpy(finalGpsData->speedKts, msgVTG->speedKts,
                sizeof(finalGpsData->speedKts)-1);
        strncpy(finalGpsData->speedKph, msgVTG->speedKph,
                sizeof(finalGpsData->speedKts)-1);
    }

    if(msgRMC->updated == true)
    {
        if(msgRMC->dataValidness == 'A')
        {
            strncpy(finalGpsData->fixTimeUTC, msgRMC->fixTimeUTC,
                    sizeof(finalGpsData->fixTimeUTC)-1);
            strncpy(finalGpsData->date, msgRMC->date,
                    sizeof(finalGpsData->date)-1);
            strncpy(finalGpsData->latitude, msgRMC->latitude,
                    sizeof(finalGpsData->latitude)-1);
            finalGpsData->ns = msgRMC->ns;
            strncpy(finalGpsData->longitude, msgRMC->longitude,
                    sizeof(finalGpsData->longitude)-1);
            finalGpsData->ew = msgRMC->ew;
            strncpy(finalGpsData->speedKts, msgRMC->speedKts,
                    sizeof(finalGpsData->speedKts)-1);
            strncpy(finalGpsData->courseOverGnd, msgRMC->courseOverGnd,
                    sizeof(finalGpsData->courseOverGnd)-1);
        }
    }
}


bool nmea_checkForGBS(char *msgToCheck)
{
    if(strstr(msgToCheck, GBS) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForGGA(char *msgToCheck)
{
    if(strstr(msgToCheck, GGA) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForGLL(char *msgToCheck)
{
    if(strstr(msgToCheck, GLL) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForGSA(char *msgToCheck)
{
    if(strstr(msgToCheck, GSA) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForRMC(char *msgToCheck)
{
    if(strstr(msgToCheck, RMC) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForVTG(char *msgToCheck)
{
    if(strstr(msgToCheck, VTG) != NULL)
        return true;

    else
        return false;
}

bool nmea_checkForZDA(char *msgToCheck)
{
    if(strstr(msgToCheck, ZDA) != NULL)
        return true;

    else
        return false;
}



uint8_t nmea_getChecksum(char *nmeaMsg)
{
    uint8_t checksum = 0;

    char *msgDup = strdup(nmeaMsg);

    strsep(&msgDup, "*\r");

    checksum = (uint8_t) strtol(strsep(&msgDup, "*\r"), NULL, 16);

    free(msgDup);

    return checksum;
}

uint8_t nmea_countChecksum(char *nmeaMsg)
{
    //XOR all bytes of the message beetween $ and * characters

    uint8_t checksum = 0;
    uint8_t i = 1;
//    uint8_t length = (uint8_t)(strlen(nmeaMsg) - 3);

//    char *test = strsep(&nmeaMsg, "$*");
//    printf("test: %s\r\n", test);

//    for(uint8_t i = 1; i < length; i++)
//        checksum ^= nmeaMsg[i];
    while(nmeaMsg[i] != '*') {
        checksum ^= nmeaMsg[i];
        i++;
    }

    return checksum;
}
