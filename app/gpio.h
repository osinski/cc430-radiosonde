/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 GPIO Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef GPIO_H
#define GPIO_H


#include "hal_gpio.h"

#include "board_defines.h"


void HAL_configureGPIO(void);

#endif //GPIO_H
