/*
 * aprs.c
 *
 *  Created on: 07.09.2018
 *      Author: dell
 */


#include "aprs.h"

void aprs_sendPacket(nmea_gpsData_t *gpsData, ax25Frame_Header_t *ax25header)
{
    ax25Frame_Data_t ax25data;
    uint8_t finalHeaderDataString[APRS_STARTFLAGS_NUMBER +
                                  2*AX25_ADDRESS_LENGTH + AX25_DIGIFIELD_LENGTH + AX25_CONTROL_LENGTH + AX25_DATA_LENGTH + AX25_FCS_LENGTH +
                                  APRS_ENDFLAGS_NUMBER + 1];	// +1 is for /0 character
    memset(finalHeaderDataString, 0, sizeof(finalHeaderDataString));

    aprs_formatGpsStructToData(gpsData, ax25data.packetData);
    aprs_formatAx25FrameToString(ax25header, &ax25data, finalHeaderDataString);
    printf("formatted aprs string: %s \r\n", finalHeaderDataString);

    aprs_toneEnum currentTone = APRS_SPACE;

//	RFM_APRS_RESET_DIO2;

    aprs_sendStartSync(currentTone);

    uint8_t i = 0;
    while(finalHeaderDataString[i] != '\0')
    {
        aprs_sendByte(finalHeaderDataString[i], &currentTone);
        i++;
    }

//	RFM_APRS_RESET_DIO2;
}

void aprs_formatGpsStructToData(nmea_gpsData_t *gpsStruct, uint8_t *aprsData)
{
//    sprintf((char*)aprsData, "!%02d%02d.%02d%c%c%03d%02d.%02d%c%c%03d/%03d /A=%dm MOS BALON",
//                            gpsStruct->latitude_degs, gpsStruct->latitude_mins, gpsStruct->latitude_hmins,
//                            gpsStruct->NSindicator != 0 ? gpsStruct->NSindicator : '?',
//                            AX25_SYMBOLTABLE_IDENTIFIER,
//                            gpsStruct->longitude_degs, gpsStruct->longitude_mins, gpsStruct->longitude_hmins,
//                            gpsStruct->EWindicator != 0 ? gpsStruct->EWindicator : '?',
//                            AX25_SYMBOLTABLE_SYMBOLCODE,
//                            gpsStruct->course, gpsStruct->speedKt, gpsStruct->altitude);
}

void aprs_createAx25Header(ax25Frame_Header_t *header)
{
	strcpy((char*)header->destAddr, APRS_DEST_ADDR);
	strcpy((char*)header->srcAddr, APRS_SRC_ADDR);
	strcpy((char*)header->digiAddr, APRS_DIGI_PATH);

	// shift all addresses bytes left by one, so their lsb are zeros
	for(uint8_t i = 0; i < AX25_ADDRESS_LENGTH; i++)
	{
		header->destAddr[i] <<= 1;
	}

	for(uint8_t i = 0; i < AX25_ADDRESS_LENGTH; i++)
	{
		header->srcAddr[i] <<= 1;

		// write 1 to lsb of last byte
		if(i == (AX25_ADDRESS_LENGTH - 2))
			header->srcAddr[i] |= 1;
	}
}

void aprs_calcAx25FrameCheckSequence(uint16_t *crc, uint8_t *string)
{
	// current crc calculation is most propably shitty as fuck and needs some more serious work

	*crc = 0xffff;

	uint8_t i = 0;
	while(string[i] != '\0')
	{
		*crc ^= string[i];

		for(uint8_t i = 0; i < 8; i++)
		{
			*crc >>= 1;

			if(*crc & 0x0001)
				*crc ^= AX25_CRC_POLYNOMIAL;
		}

		i++;
	}
}

void aprs_formatAx25FrameToString(ax25Frame_Header_t *header, ax25Frame_Data_t *data, uint8_t *resultString)
{
	char temp[] = {'0', '\0'};

	sprintf(temp, "%c", AX25_START_END_FLAG);

	strcpy((char*)resultString, (const char*)temp);
	strcat((char*)resultString, (const char*)header->destAddr);
	strcat((char*)resultString, (const char*)header->srcAddr);
	strcat((char*)resultString, (const char*)header->digiAddr);

	sprintf(temp, "%c", AX25_CONTROL_FIELD);
	strcat((char*)resultString, (const char*)temp);
	sprintf(temp, "%c", AX25_PROTOCOL_ID);
	strcat((char*)resultString, (const char*)temp);

	strcat((char*)resultString, (const char*)data->packetData);

	aprs_calcAx25FrameCheckSequence(&(data->frameCheckSequence), resultString);

	sprintf(temp, "%c", ((uint8_t) data->frameCheckSequence) ^ 0xff);
	strcat((char*)resultString, (const char*)temp);
	sprintf(temp, "%c", ((uint8_t) data->frameCheckSequence >> 8) ^ 0xff);
	strcat((char*)resultString, (const char*)temp);

	sprintf(temp, "%c", AX25_START_END_FLAG);
	strcat((char*)resultString, (const char*)temp);
}

void aprs_sendStartSync(aprs_toneEnum currentTone)
{
	for(uint8_t i = 0; i < 10; i++)
	{
		aprs_handleRfmDio2Generation(currentTone);
	}
}

void aprs_sendByte(uint8_t byte, aprs_toneEnum *currentTone)
{
	uint8_t bitStuffingCounter = 0;			// bit stuffing - if more than 5 '1' in a row are to be sent, insert 0 after 5th
	uint8_t bit = 0;

	if(byte == 0x7e)
	{
		for(uint8_t i = 0; i < 8; i++)
		{
			bit = byte & 1;

			if(bit == 0)
				toggleTone(currentTone);

			aprs_handleRfmDio2Generation(*currentTone);

			byte >>= 1;
		}
	}
	else
	{
		for(uint8_t j = 0; j < 8; j++)
		{
			bit = byte & 1;

			if(bit == 1)
			{
				bitStuffingCounter++;

				if(bitStuffingCounter == 5)
				{
					toggleTone(currentTone);

					bitStuffingCounter = 0;
				}
			}
			else
			{
				bitStuffingCounter = 0;
				toggleTone(currentTone);
			}

			aprs_handleRfmDio2Generation(*currentTone);

			byte >>= 1;
		}
	}
}

//void aprs_handleRfmDio2Generation(aprs_toneEnum currentTone)
//{
//	uint8_t repeatsNumber = (float)APRS_TONE_DURATION / (float)(currentTone == APRS_MARK ? APRS_MARK_TOGGLE_DURATION : APRS_SPACE_TOGGLE_DURATION) + 0.5;

//	timers_aprsToneTypeTimerStart(currentTone == APRS_MARK ? APRS_MARK_TOGGLE_DURATION : APRS_SPACE_TOGGLE_DURATION);

//	for(uint8_t i = 0; i < repeatsNumber; i++)
//	{
//		if(currentTone == APRS_SPACE)
//			timers_aprsToneTypeDelay(APRS_SPACE_TOGGLE_DURATION);
//		else
//			timers_aprsToneTypeDelay(APRS_MARK_TOGGLE_DURATION);

//		RFM_APRS_TOGGLE_DIO2;
//	}

//	timers_aprsToneTypeTimerStop();
//}

void aprs_setRadio(rf1a_settings_t *radioConfig)
{
//    radioConfig->regDataModul.datamode = DATAMODE_CONTINUOUS;
//    radioConfig->regDataModul.modulationShaping = MODULATIONSHAPING_00;
//	radioConfig->regDataModul.modulationType = MODULATIONTYPE_FSK;
//	rfm69_writeDataModulReg(radioConfig);

//    radioConfig->carrierFreq_kHz = APRS_CARRIER_FREQ;
//    rfm69_writeCarrierFreqRegs(radioConfig);

//    radioConfig->freqDeviation_kHz = 3125;
//    rfm69_writeFreqDevRegs(radioConfig);

//    radioConfig->bitrate_bps = 19200;
//    rfm69_writeBitRateRegs(radioConfig);

//    radioConfig->regOpMode.sequencer = SEQUENCER_ON;
//    radioConfig->regOpMode.opmode = OPMODE_TRANSMITTER;
//    rfm69_writeOpModeReg(radioConfig);
}

