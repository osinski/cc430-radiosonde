#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "printf_with_colors.h"

#include "rysio_defs.h"

#include "nmeaParser.h"
#include "ubloxNeo6m.h"
#include "rtty.h"
#include "aprs.h"
#include "radiosonde_control.h"

#include "radio.h"
#include "gpio.h"
#include "system.h"
#include "timer.h"
#include "serial_comm.h"



int main(void)
{
    extern gpio_handler_t gpio_led;
//    extern gpio_handler_t gpio_gpsCS;
    extern gpio_handler_t gpio_gpsExtint;

//    extern tim_captureCompareHandler_t timer_radioSignalCaptureCompare;

    extern usci_spi_handler_t usci_ab_spi_handler;

    extern controlFlags_t system_Ctrl;
    extern uartCommandFlags_t system_UartCmds;

    extern uint8_t ubloxBuf[];

    nmea_gpsData_t gpsData = {
        .currTimeUTC = "213700\0",
        .fixTimeUTC = "213730\0",
        .date = "251119\0",
        .latitude = "\0",
        .ns = 'N',
        .longitude = "\0",
        .ew = 'E',
        .fixStatus = 'A',
        .mslAltitude = "123.4\0",
        .dataValidness = '6',
        .speedKts = "120.5\0",
        .speedKph = "123.3\0",
        .courseOverGnd = "178.4\0"
    };


    wdt_holdWatchdog();


    HAL_configureSystemPower();
    HAL_configureClocks();

    HAL_initSystemTimer();
    HAL_initAuxillaryTimer();
    HAL_initRTC();

    HAL_configureGPIO();

    HAL_configureSerialComm();

    HAL_configureRadio();

    ublox_initialize();

    printf(ANSI_CODE_BOLD); printf(ANSI_CODE_BLUE);
    printf("\r\nRADIOSONDE HELLO WORLD\r\n");
    printf(ANSI_CODE_RESET);

    __enable_interrupt();

    while(1) {
        if(system_Ctrl.radioEnabled == true) {
            if (!system_Ctrl.txPending && system_Ctrl.txEnabled) {
                system_Ctrl.txPending = true;

                rf1a_changeMachineState(RF1A_STATE_FS_MANCAL);
                __delay_cycles(5000);
                rf1a_changeMachineState(RF1A_STATE_IDLE);

                gpio_setHigh(&gpio_led);

                rtty_sendPacket(&gpsData);

                gpio_setLow(&gpio_led);

                rf1a_changeMachineState(RF1A_STATE_SLEEP);

                system_Ctrl.txPending = false;
                system_Ctrl.txEnabled = false;
            }
        }

        if(system_Ctrl.uartCmdsEnabled == true) {
            if(system_UartCmds.command_1 == true) {
                ublox_readData();

//                uint8_t ubloxbuf[] = "$GPVTG,77.52,T,,M,0.004,N,0.008,K,A*06\r\n\
//$GPRMC,083559.00,A,4717.11437,N,00833.91522,E,0.004,77.52,091202,,,A*57\r\n\
//$GPGLL,4717.11364,N,00833.91565,E,092321.00,A,A*60\r\n\
//$GPGGA,092725.00,4717.11399,N,00833.91590,E,1,8,1.01,499.6,M,48.0,M,,0*5B";


                printf(ANSI_CODE_YELLOW);
                printf("%s\r\n", ubloxBuf);
                printf(ANSI_CODE_RESET);

                nmea_parse((char *)ubloxBuf, &gpsData);

                system_UartCmds.command_1 = false;
            }

            if(system_UartCmds.command_2 == true) {
                uint8_t pinState = READ_BIT(P2OUT, (1<<0));
                if(pinState) {
                    printf("Pulling Extint LOW\r\n");
                    gpio_setLow(&gpio_gpsExtint);
                } else {
                    printf("Pulling Extint HIGH\r\n");
                    gpio_setHigh(&gpio_gpsExtint);
                }

                system_UartCmds.command_2 = false;
            }

            if(system_UartCmds.command_3 == true) {
                if(system_Ctrl.radioEnabled == true) {
                    printf("Disabling radio\r\n");

                    system_Ctrl.radioEnabled = false;
                } else {
                    printf("Enabling Radio\r\n");

                    system_Ctrl.radioEnabled = true;
                }

                system_UartCmds.command_3 = false;
            }

            if(system_UartCmds.command_4 == true) {
                printf("Entering LPM3...\r\n");

                HAL_triggerRTC();
                _BIS_SR(LPM3_bits + GIE);

                system_UartCmds.command_4 = false;
            }

            if(system_UartCmds.command_5 == true) {
                printf("Sending ublox initialization\r\n");
                ublox_initialize();

                system_UartCmds.command_5 = false;
            }
        }
    }
}
