#include <cc430f5137.h>


#include "rysio_defs.h"

#include "hal_timer_a.h"
#include "hal_gpio.h"
#include "hal_usci_common.h"

#include "system.h"
#include "timer.h"
#include "printf_with_colors.h"



extern timerTimeoutFlags_t timeoutFlags;
extern controlFlags_t system_Ctrl;
extern uartCommandFlags_t system_UartCmds;


void __interrupt_vec(TIMER0_A0_VECTOR) TIMER0_A0_ISR(void)
{
    system_Ctrl.txEnabled = true;

    CLEAR_BIT(TA0CCTL0, CCIFG);
}

void __interrupt_vec(TIMER1_A0_VECTOR) TIMER1_A0_ISR(void)
{
    timeoutFlags.rttyToneDurationPeriodElapsed = true;
}

void __interrupt_vec(USCI_A0_VECTOR) USCI_A_ISR(void)
{
    uint8_t intFlags = usci_readInterruptFlags(USCI_A_0);
    if(intFlags & USCI_INTERRUPTS_RX) {
        char byte = usci_readRxBuf(USCI_A_0);

        switch (byte) {
        case '1':
            system_UartCmds.command_1 = true;
            break;
        case '2':
            system_UartCmds.command_2 = true;
            break;
        case '3':
            system_UartCmds.command_3 = true;
            break;
        case '4':
            system_UartCmds.command_4 = true;
            break;
        case '5':
            system_UartCmds.command_5 = true;
            break;
        }

        usci_clearInterruptFlag(USCI_A_0, USCI_INTERRUPTS_RX);

    } else if(intFlags & USCI_INTERRUPTS_TX) {
        usci_clearInterruptFlag(USCI_A_0, USCI_INTERRUPTS_TX);
    }
}

void __interrupt_vec(RTC_A_VECTOR) RTC_A_ISR(void)
{
    rtc_a_clearInterruptFlag(RTC_INTERRUPT_TIME_EVENT);

    printf("Exiting Low Power\r\n");
    HAL_haltRTC();
    _BIC_SR_IRQ(LPM3_bits);
}

