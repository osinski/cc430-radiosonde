/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 SYStem Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef SYSTEM_H
#define SYSTEM_H

#include <cc430f5137.h>

#include "hal_pmm.h"
#include "hal_ucs.h"
#include "hal_gpio.h"
#include "hal_wdt_a.h"
#include "hal_timer_a.h"


typedef struct
{
//  misc counters
    uint32_t totalPacketCount;
    uint8_t tmpPacketCount;

    uint8_t sleepIntervalSeconds;

//  Feature enable flags
    bool uartCmdsEnabled;
    bool radioEnabled;
    bool sendRTTY;
    bool sendAPRS;
    bool parseNMEA;

//  Control/status flags
    bool txEnabled;
    bool txPending;

    bool standby;

} controlFlags_t;

typedef struct
{
    bool command_1;
    bool command_2;
    bool command_3;
    bool command_4;
    bool command_5;

} uartCommandFlags_t;


void HAL_configureClocks(void);
void HAL_configureSystemPower(void);
void HAL_initSystemTimer(void);

#endif //SYSTEM_H
