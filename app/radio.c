/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Radio Settings
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#include "radio.h"

// PA table for 0 dBm
#define PA_TABLE {0x50,0x00,0x00,0x00,0x00,0x00,0x00,0x00}

// Address Config = No address check
// Base Frequency = 432.999817
// CRC Autoflush = false
// CRC Enable = false
// Carrier Frequency = 432.999817
// Channel Number = 0
// Channel Spacing = 199.951172
// Data Format = Asynchronous serial mode
// Data Rate = 0.0499785
// Deviation = 1.586914
// Device Address = 0
// Manchester Enable = false
// Modulated = true
// Modulation Format = 2-GFSK
// PA Ramping = false
// Packet Length = 255
// Packet Length Mode = Infinite packet length mode
// Preamble Count = 4
// RX Filter BW = 58.035714
// Sync Word Qualifier Mode = No preamble/sync
// TX Power = 0
// Whitening = false
// PA table

// Rf settings for testing
static rf1a_settings_t rfSettings_test = {
    0x0B,  // IOCFG2       GDO2 Output Configuration
    0x3F,  // IOCFG1       GDO1 Output Configuration    - RFCLK/192
    0x2E,  // IOCFG0       GDO0 Output Configuration    - Tri-State
    0x47,  // FIFOTHR      RX FIFO and TX FIFO Thresholds
    0xD3,  // SYNC1        Sync Word, High Byte
    0x91,  // SYNC0        Sync Word, Low Byte
    0xFF,  // PKTLEN       Packet Length
    0x04,  // PKTCTRL1     Packet Automation Control
    0x32,  // PKTCTRL0     Packet Automation Control
    0x00,  // ADDR         Device Address
    0x00,  // CHANNR       Channel Number
    0x06,  // FSCTRL1      Frequency Synthesizer Control
    0x00,  // FSCTRL0      Frequency Synthesizer Control
    0x10,  // FREQ2        Frequency Control Word, High Byte
    0xA7,  // FREQ1        Frequency Control Word, Middle Byte
    0x62,  // FREQ0        Frequency Control Word, Low Byte
    0xF1,  // MDMCFG4      Modem Configuration
    0x02,  // MDMCFG3      Modem Configuration
    0x10,  // MDMCFG2      Modem Configuration
    0x22,  // MDMCFG1      Modem Configuration
    0xF8,  // MDMCFG0      Modem Configuration
    0x00,  // DEVIATN      Modem Deviation Setting
    0x07,  // MCSM2        Main Radio Control State Machine Configuration
    0x30,  // MCSM1        Main Radio Control State Machine Configuration
    0x10,  // MCSM0        Main Radio Control State Machine Configuration
    0x16,  // FOCCFG       Frequency Offset Compensation Configuration
    0x6C,  // BSCFG        Bit Synchronization Configuration
    0x03,  // AGCCTRL2     AGC Control
    0x40,  // AGCCTRL1     AGC Control
    0x91,  // AGCCTRL0     AGC Control
    0x80,  // WOREVT1      High Byte Event0 Timeout
    0x00,  // WOREVT0      Low Byte Event0 Timeout
    0xFB,  // WORCTRL      Wake On Radio Control
    0x56,  // FREND1       Front End RX Configuration
    0x10,  // FREND0       Front End TX Configuration
    0xE9,  // FSCAL3       Frequency Synthesizer Calibration
    0x2A,  // FSCAL2       Frequency Synthesizer Calibration
    0x00,  // FSCAL1       Frequency Synthesizer Calibration
    0x1F,  // FSCAL0       Frequency Synthesizer Calibration
    0x59,  // FSTEST       Frequency Synthesizer Calibration Control
    0x7F,  // PTEST        Production Test
    0x3F,  // AGCTEST      AGC Test
    0x81,  // TEST2        Various Test Settings
    0x35,  // TEST1        Various Test Settings
    0x09,  // TEST0        Various Test Settings
};


// Address Config = No address check
// Base Frequency = 433.999969
// CRC Autoflush = false
// CRC Enable = false
// Carrier Frequency = 433.999969
// Channel Number = 0
// Channel Spacing = 25.390625
// Data Format = Asynchronous serial mode
// Data Rate = 1.19948
// Deviation = 5.157471
// Device Address = 0
// Manchester Enable = false
// Modulation Format = ASK/OOK
// PA Ramping = false
// Packet Length = 255
// Packet Length Mode = Infinite packet length mode
// Preamble Count = 4
// RX Filter BW = 58.035714
// Sync Word Qualifier Mode = No preamble/sync
// TX Power = 0
// Whitening = false

// Rf settings for OOK test
rf1a_settings_t rfSettings_ooktest = {
    0x0B,  // IOCFG2       GDO2 Output Configuration
    0x2E,  // IOCFG1       GDO1 Output Configuration
    0x2E,  // IOCFG0       GDO0 Output Configuration
    0x47,  // FIFOTHR      RX FIFO and TX FIFO Thresholds
    0xD3,  // SYNC1        Sync Word, High Byte
    0x91,  // SYNC0        Sync Word, Low Byte
    0xFF,  // PKTLEN       Packet Length
    0x04,  // PKTCTRL1     Packet Automation Control
    0x32,  // PKTCTRL0     Packet Automation Control
    0x00,  // ADDR         Device Address
    0x00,  // CHANNR       Channel Number
    0x06,  // FSCTRL1      Frequency Synthesizer Control
    0x00,  // FSCTRL0      Frequency Synthesizer Control
    0x10,  // FREQ2        Frequency Control Word, High Byte
    0xB1,  // FREQ1        Frequency Control Word, Middle Byte
    0x3B,  // FREQ0        Frequency Control Word, Low Byte
    0xF5,  // MDMCFG4      Modem Configuration
    0x83,  // MDMCFG3      Modem Configuration
    0x30,  // MDMCFG2      Modem Configuration
    0x20,  // MDMCFG1      Modem Configuration
    0x00,  // MDMCFG0      Modem Configuration
    0x15,  // DEVIATN      Modem Deviation Setting
    0x07,  // MCSM2        Main Radio Control State Machine Configuration
    0x30,  // MCSM1        Main Radio Control State Machine Configuration
    0x10,  // MCSM0        Main Radio Control State Machine Configuration
    0x16,  // FOCCFG       Frequency Offset Compensation Configuration
    0x6C,  // BSCFG        Bit Synchronization Configuration
    0x03,  // AGCCTRL2     AGC Control
    0x40,  // AGCCTRL1     AGC Control
    0x91,  // AGCCTRL0     AGC Control
    0x80,  // WOREVT1      High Byte Event0 Timeout
    0x00,  // WOREVT0      Low Byte Event0 Timeout
    0xFB,  // WORCTRL      Wake On Radio Control
    0x56,  // FREND1       Front End RX Configuration
    0x11,  // FREND0       Front End TX Configuration
    0xE9,  // FSCAL3       Frequency Synthesizer Calibration
    0x2A,  // FSCAL2       Frequency Synthesizer Calibration
    0x00,  // FSCAL1       Frequency Synthesizer Calibration
    0x1F,  // FSCAL0       Frequency Synthesizer Calibration
    0x59,  // FSTEST       Frequency Synthesizer Calibration Control
    0x7F,  // PTEST        Production Test
    0x3F,  // AGCTEST      AGC Test
    0x81,  // TEST2        Various Test Settings
    0x35,  // TEST1        Various Test Settings
    0x09,  // TEST0        Various Test Settings
};

rf1a_settings_t rfSettings_rx_test = {
    0x0B,  // IOCFG2       GDO2 Output Configuration
    0x2E,  // IOCFG1       GDO1 Output Configuration
    0x0C,  // IOCFG0       GDO0 Output Configuration
    0x47,  // FIFOTHR      RX FIFO and TX FIFO Thresholds
    0xD3,  // SYNC1        Sync Word, High Byte
    0x91,  // SYNC0        Sync Word, Low Byte
    0xFF,  // PKTLEN       Packet Length
    0x04,  // PKTCTRL1     Packet Automation Control
    0x12,  // PKTCTRL0     Packet Automation Control
    0x00,  // ADDR         Device Address
    0x00,  // CHANNR       Channel Number
    0x06,  // FSCTRL1      Frequency Synthesizer Control
    0x00,  // FSCTRL0      Frequency Synthesizer Control
    0x10,  // FREQ2        Frequency Control Word, High Byte
    0xB1,  // FREQ1        Frequency Control Word, Middle Byte
    0x3B,  // FREQ0        Frequency Control Word, Low Byte
    0xF5,  // MDMCFG4      Modem Configuration
    0x83,  // MDMCFG3      Modem Configuration
    0x00,  // MDMCFG2      Modem Configuration
    0x22,  // MDMCFG1      Modem Configuration
    0xF8,  // MDMCFG0      Modem Configuration
    0x15,  // DEVIATN      Modem Deviation Setting
    0x07,  // MCSM2        Main Radio Control State Machine Configuration
    0x30,  // MCSM1        Main Radio Control State Machine Configuration
    0x10,  // MCSM0        Main Radio Control State Machine Configuration
    0x16,  // FOCCFG       Frequency Offset Compensation Configuration
    0x6C,  // BSCFG        Bit Synchronization Configuration
    0x03,  // AGCCTRL2     AGC Control
    0x40,  // AGCCTRL1     AGC Control
    0x91,  // AGCCTRL0     AGC Control
    0x80,  // WOREVT1      High Byte Event0 Timeout
    0x00,  // WOREVT0      Low Byte Event0 Timeout
    0xFB,  // WORCTRL      Wake On Radio Control
    0x56,  // FREND1       Front End RX Configuration
    0x10,  // FREND0       Front End TX Configuration
    0xE9,  // FSCAL3       Frequency Synthesizer Calibration
    0x2A,  // FSCAL2       Frequency Synthesizer Calibration
    0x00,  // FSCAL1       Frequency Synthesizer Calibration
    0x1F,  // FSCAL0       Frequency Synthesizer Calibration
    0x59,  // FSTEST       Frequency Synthesizer Calibration Control
    0x7F,  // PTEST        Production Test
    0x3F,  // AGCTEST      AGC Test
    0x81,  // TEST2        Various Test Settings
    0x35,  // TEST1        Various Test Settings
    0x09,  // TEST0        Various Test Settings
};

#define PA_TABLE {0x50,0x00,0x00,0x00,0x00,0x00,0x00,0x00}

void HAL_configureRadio(void)
{
//    rf1a_writeRfSettings(&rfSettings_test);
    rf1a_writeRfSettings(&rfSettings_ooktest);
    rf1a_writeSinglePATable(0x51);
}
