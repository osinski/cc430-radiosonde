/*
 * aprs.h
 *
 *  Created on: 07.09.2018
 *      Author: dell
 */

//	http://www.aprs.org/doc/APRS101.PDF

#pragma once

#include "stdlib.h"
#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"

#include "radiosonde_config.h"
#include "nmeaParser.h"
#include "hal_rf1a.h"
#include "timer.h"

#define AX25_ADDRESS_LENGTH				8		// 7 + /0
#define AX25_DIGIFIELD_LENGTH			8		// max 64 (8 relays with 7 bytes (+ 0) each)
#define AX25_DATA_LENGTH				63		// page 94, bottom frame, link to document at the top of file
#define AX25_CONTROL_LENGTH				2
#define AX25_FCS_LENGTH					2

#define AX25_START_END_FLAG				0x7E
#define AX25_CONTROL_FIELD				0x03
#define AX25_PROTOCOL_ID				0xF0
#define AX25_DATA_TYPE					'!'			// 1 byte at the beginning of data field
#define AX25_SYMBOLTABLE_IDENTIFIER		'/'
#define AX25_SYMBOLTABLE_SYMBOLCODE		'O'			// symbol 'O' from primary symbol table ('/') means balloon
#define AX25_CRC_POLYNOMIAL				0x8408		// or 0x1021?

#define toggleTone(currentTone)		((*currentTone) = (*currentTone) == APRS_MARK ? APRS_SPACE : APRS_MARK)

typedef enum
{
	APRS_MARK,
	APRS_SPACE

} aprs_toneEnum;

typedef struct
{
	uint8_t destAddr[AX25_ADDRESS_LENGTH];
	uint8_t srcAddr[AX25_ADDRESS_LENGTH];
	uint8_t digiAddr[AX25_DIGIFIELD_LENGTH];

} ax25Frame_Header_t;

typedef struct
{
	uint8_t packetData[AX25_DATA_LENGTH];
	uint16_t frameCheckSequence;

} ax25Frame_Data_t;

void aprs_createAx25Header(ax25Frame_Header_t *header);
void aprs_formatGpsStructToData(nmea_gpsData_t *gpsStruct, uint8_t *aprsData);
void aprs_calcAx25FrameCheckSequence(uint16_t *crc, uint8_t *string);
void aprs_formatAx25FrameToString(ax25Frame_Header_t *header, ax25Frame_Data_t *data, uint8_t *resultString);
void aprs_sendStartSync(aprs_toneEnum currentTone);
void aprs_sendByte(uint8_t byte, aprs_toneEnum *currentTone);
void aprs_handleRfmDio2Generation(aprs_toneEnum currentTone);

void aprs_sendPacket(nmea_gpsData_t *gpsData, ax25Frame_Header_t *ax25header);

void aprs_setRadio(rf1a_settings_t *radioConfig);
