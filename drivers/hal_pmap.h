/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Port Mappings Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/


#ifndef HAL_PMAP_H
#define HAL_PMAP_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"


// NOTE: due to gpio driver implementation it is required to use it
// otherwise only one mapping could be performed
static inline void pmap_enableRuntimeConfiguration(void)
{
    SET_BIT(PMAPCTL, PMAPRECFG);
}

static inline void pmap_disableRuntimeConfiguration(void)
{
    CLEAR_BIT(PMAPCTL, PMAPRECFG);
}

static inline void pmap_unlockRegisters(void)
{
    WRITE_REG(PMAPKEYID, PMAPKEY);
}

static inline void pmap_lockRegisters(void)
{
    WRITE_REG(PMAPKEYID, 0);
}

static inline void pmap_setMapping_P1(gpio_pins_t pin, pmap_mappings_t mapping)
{
    WRITE_REG(HWREG8(P1MAP_BASE + pin), mapping);
}

static inline void pmap_setMapping_P2(gpio_pins_t pin, pmap_mappings_t mapping)
{
    WRITE_REG(HWREG8(P2MAP_BASE + pin), mapping);
}

static inline void pmap_setMapping_P3(gpio_pins_t pin, pmap_mappings_t mapping)
{
    WRITE_REG(HWREG8(P3MAP_BASE + pin), mapping);
}


#endif // PMAP_H
