/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * Useful Defines, Macros etc.
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef RYSIO_DEFS_H
#define RYSIO_DEFS_H

// suppres qtcreator clang warnings about unused statc inline functions
#define inline inline __attribute__((unused))

/*  Stolen from STM32 CMSIS ***************************************************/
#define SET_BIT(REG, BIT)     ((REG) |= (BIT))
#define CLEAR_BIT(REG, BIT)   ((REG) &= ~(BIT))
#define TOGGLE_BIT(REG, BIT)  ((REG) ^= (BIT))
#define READ_BIT(REG, BIT)    ((REG) & (BIT))
#define CLEAR_REG(REG)        ((REG) = (0x0))
#define WRITE_REG(REG, VAL)   ((REG) = (VAL))
#define READ_REG(REG)         ((REG))
#define MODIFY_REG(REG, CLEARMASK, SETMASK)  WRITE_REG((REG), (((READ_REG(REG)) & (~(CLEARMASK))) | (SETMASK)))


#define HWREG32(x)              (*((volatile uint32_t *)((uint16_t)x)))
#define HWREG16(x)              (*((volatile uint16_t *)((uint16_t)x)))
#define HWREG8(x)               (*((volatile uint8_t *)((uint16_t)x)))

#define st(x)                                           do { x } while (__LINE__ == -1)
#define ENTER_CRITICAL_SECTION(x)       st(x = __get_interrupt_state(); __disable_interrupt(); )
#define EXIT_CRITICAL_SECTION(x)    __set_interrupt_state(x)


#define __delay_cycles(x) \
    ({ \
         volatile unsigned long int j; \
         for(j = 0; j < x; j++) \
         { \
             __no_operation(); \
         } \
     })


#endif
