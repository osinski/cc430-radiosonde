/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 SYStem Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_SYS_H
#define HAL_SYS_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"

typedef struct
{
    // info block only
    uint8_t info_length;
    uint8_t crc_length;
    uint16_t crc_value;

    uint8_t device_id;

    uint8_t hardware_revision;
    uint8_t firmware_revision;

    bool valid;

} sys_deviceDescriptorTable_t;

extern sys_deviceDescriptorTable_t deviceDescrTable;

bool sys_readDescriptorTable(sys_deviceDescriptorTable_t *ddt);

static inline void sys_setLowPowerMode0(void)
{
    __bis_SR_register(LPM0_bits);
}

// won't work in ISR -> SP overwrites SR after RETI
static inline void sys_exitLowPowerMode0(void)
{
    __bic_SR_register(LPM0_bits);
}

static inline void sys_setLowPowerMode1(void)
{
    __bis_SR_register(LPM1_bits);
}

// won't work in ISR -> SP overwrites SR after RETI
static inline void sys_exitLowPowerMode1(void)
{
//    __bic_SR_register(LPM1_bits);
    _BIC_SR_IRQ(LPM1_bits);
}

static inline void sys_setLowPowerMode2(void)
{
    __bis_SR_register(LPM2_bits);
}

// won't work in ISR -> SP overwrites SR after RETI
static inline void sys_exitLowPowerMode2(void)
{
//    __bic_SR_register(LPM2_bits);
    _BIC_SR_IRQ(LPM2_bits);
}

static inline void sys_setLowPowerMode3(void)
{
    __bis_SR_register(LPM3_bits);
}

// won't work in ISR -> SP overwrites SR after RETI
static inline void sys_exitLowPowerMode3(void)
{
//    __bic_SR_register(LPM3_bits);
    _BIC_SR_IRQ(LPM3_bits);
}

static inline void sys_setLowPowerMode4(void)
{
    __bis_SR_register(LPM4_bits);
}

// won't work in ISR -> SP overwrites SR after RETI
static inline void sys_exitLowPowerMode4(void)
{
//    __bic_SR_register(LPM4_bits);
    _BIC_SR_IRQ(LPM4_bits);
}

static inline uint16_t sys_readControlReg(void)
{
    return READ_REG(SYSCTL);
}

static inline uint16_t sys_readUserNMIVectorReg(void)
{
    return READ_REG(SYSUNIV);
}

static inline uint16_t sys_readSystemNMIVectorReg(void)
{
    return READ_REG(SYSSNIV);
}

/* Read to identify the last cause of reset */
static inline uint16_t sys_readResetInterruptVectorReg(void)
{
    return READ_REG(SYSRSTIV);
}

#endif //HAL_SYS_H
