#include "hal_usci_common.h"

usci_clock_handler_t usci_ab_clockSettings = {
    .clock = USCI_CLOCK_EXTERNAL,
    .baudrate_prescalers.baudrate = 0x0000
};

void usci_clock_init(usci_peripherals_t periph, usci_clock_handler_t *clk)
{
    usci_setClock(periph, clk->clock);
    usci_writeBaudRateSetting(periph, clk->baudrate_prescalers);
}
