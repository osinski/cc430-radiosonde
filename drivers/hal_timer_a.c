#include "hal_timer_a.h"

void tim_setCaptureCompareConfig(tim_captureCompareHandler_t *cc)
{
    uint16_t regval, regmask;

    regval = cc->mode | cc->outputMode | cc->captureMode |
             cc->inputSelect | cc->synchronizeCaptureSource;

    WRITE_REG(HWREG16(cc->timInstance + cc->ccInstance), regval);
}

void tim_setTimConfig(tim_timerHandler_t *tim)
{
    uint16_t regval, regmask;

    regval = tim->clockSource | tim->modeControl |
             tim->inputDivider | tim->inputDividerEx;

    WRITE_REG(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), regval);

}

tim_captureCompareHandler_t tim_newDefaultCaptureCompare(tim_timerInstance_t timInstance,
                                  tim_captureCompareInstance_t ccInstance)
{
    tim_captureCompareHandler_t cc;

    cc.timInstance = timInstance;
    cc.ccInstance = ccInstance;

    cc.mode = TIM_CC_MODE_DEFAULT;
    cc.outputMode = TIM_CC_OUTPUTMODE_DEFAULT;
    cc.captureMode = TIM_CC_CAPTURE_MODE_DEFAULT;
    cc.inputSelect = TIM_CC_INPUT_DEFAULT;
    cc.synchronizeCaptureSource = TIM_CC_CAPTURE_DEFAULT;

    return cc;
}

tim_timerHandler_t tim_newDefaultTimer(tim_timerInstance_t instance)
{
    tim_timerHandler_t tim;

    tim.instance = instance;
    tim.clockSource = TIM_CLOCKSOURCE_DEFAULT;
    tim.modeControl = TIM_MODE_DEFAULT;
    tim.inputDivider = TIM_INPUTDIVIDER_DEFAULT;
    tim.inputDividerEx = TIM_INPUTDIVIDER_EX_DEFAULT;

    return tim;
}
