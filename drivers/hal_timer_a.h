/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 TIMER_A Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_TIMER_A_H
#define HAL_TIMER_A_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "hal_common_defs.h"
#include "rysio_defs.h"



typedef struct
{
    tim_timerInstance_t timInstance;
    tim_captureCompareInstance_t ccInstance;

    tim_ccCaptureModes_t captureMode;
    tim_ccInputs_t inputSelect;
    tim_ccSynchronization_t synchronizeCaptureSource;
    tim_ccMode_t mode;
    tim_ccOutputMode_t outputMode;

} tim_captureCompareHandler_t;

void tim_setCaptureCompareConfig(tim_captureCompareHandler_t *cc);

tim_captureCompareHandler_t tim_newDefaultCaptureCompare(tim_timerInstance_t timInstance,
                                  tim_captureCompareInstance_t ccInstance);

typedef struct
{
    tim_timerInstance_t instance;

    tim_clockSources_t clockSource;
    tim_inputDividers_t inputDivider;
    tim_inputDividers_t inputDividerEx;
    tim_modes_t modeControl;

} tim_timerHandler_t;

void tim_setTimConfig(tim_timerHandler_t *tim);

tim_timerHandler_t tim_newDefaultTimer(tim_timerInstance_t instance);

/***    Main Timer Control Stuff    *******************************************/


static inline void tim_enableMainInterrupts(tim_timerHandler_t *tim)
{
    SET_BIT(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), TAIE);
}

static inline void tim_disableMainInterrupts(tim_timerHandler_t *tim)
{
    CLEAR_BIT(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), TAIE);
}


static inline bool tim_isMainInterruptPending(tim_timerHandler_t *tim)
{
    return (bool) READ_BIT(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), TAIFG);
}


static inline void tim_clearTimer(tim_timerHandler_t *tim)
{
    SET_BIT(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), TACLR);
}

static inline void tim_setCountReg(tim_timerHandler_t *tim, uint16_t val)
{
    WRITE_REG(HWREG16(tim->instance + OFFSET_TIM_TIMER_VALUEREG), val);
}


static inline uint16_t tim_readCountReg(tim_timerHandler_t *tim)
{
    return READ_REG(HWREG16(tim->instance + OFFSET_TIM_TIMER_VALUEREG));
}

static inline
void tim_setTimerMode(tim_timerHandler_t *tim, tim_modes_t mode)
{
    MODIFY_REG(HWREG16(tim->instance + OFFSET_TIM_TIMER_CONTROLREG), MC0 | MC1,
               mode);
}

static inline
void tim_haltTimer(tim_timerHandler_t *tim)
{
    tim_setTimerMode(tim, TIM_MODE_STOP);
}

static inline
void tim_reenableTimer(tim_timerHandler_t *tim)
{
    tim_setTimerMode(tim, tim->modeControl);
}


/****   Capture Compare Stuff   ***********************************************/

static inline void tim_OUTbit_set(tim_captureCompareHandler_t *cc)
{
    SET_BIT(HWREG16(cc->timInstance + cc->ccInstance), OUT);
}

static inline void tim_OUTbit_clear(tim_captureCompareHandler_t *cc)
{
    CLEAR_BIT(HWREG16(cc->timInstance + cc->ccInstance), OUT);
}

static inline void tim_OUTbit_toggle(tim_captureCompareHandler_t *cc)
{
    TOGGLE_BIT(HWREG16(cc->timInstance + cc->ccInstance), OUT);
}

static inline void tim_enableCaptureCompareInterrupts(tim_captureCompareHandler_t *cc)
{
    SET_BIT(HWREG16(cc->timInstance + cc->ccInstance), CCIE);
}

static inline void tim_disableCaptureCompareInterrupts(tim_captureCompareHandler_t *cc)
{
    CLEAR_BIT(HWREG16(cc->timInstance + cc->ccInstance), CCIE);
}


static inline bool tim_isCaptureCompareInterruptPending(tim_captureCompareHandler_t *cc)
{
    return (bool) READ_BIT(HWREG16(cc->timInstance + cc->ccInstance), CCIFG);
}


static inline void tim_setCaptureCompareReg(tim_captureCompareHandler_t *cc,
                                     uint16_t val)
{
    WRITE_REG(HWREG16(cc->timInstance + cc->ccInstance + 0x10), val);
}

static inline uint16_t tim_readCaptureCompareReg(tim_captureCompareHandler_t *cc)
{
    return READ_REG(HWREG16(cc->timInstance + cc->ccInstance + 0x10));
}

static inline uint16_t tim_readCaptureCompareInput(tim_captureCompareHandler_t *cc)
{
    return READ_BIT(HWREG16(cc->timInstance + cc->ccInstance), CCI);
}

static inline uint16_t tim_readCaptureCompareSynchronizedInput(tim_captureCompareHandler_t *cc)
{
    return READ_BIT(HWREG16(cc->timInstance + cc->ccInstance), SCCI);
}


static inline void tim_setCaptureCompareOutput(tim_captureCompareHandler_t *cc,
                                        uint8_t lowhigh)
{
    MODIFY_REG(HWREG16(cc->timInstance + cc->ccInstance), OUT, lowhigh ? OUT : 0);
}


static inline uint16_t tim_readCaptureCompareOverflow(tim_captureCompareHandler_t *cc)
{
    return READ_BIT(HWREG16(cc->timInstance + cc->ccInstance), COV);
}


#endif // TIMER_A_H
