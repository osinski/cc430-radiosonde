/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 CRC Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_CRC_H
#define HAL_CRC_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"


static inline
void crc_initWithSeed(uint16_t seed)
{
    WRITE_REG(CRCINIRES, seed);
}

static inline
void crc_writeData(uint16_t data)
{
    WRITE_REG(CRCDI, data);
}

static inline
uint16_t crc_readData(void)
{
    return READ_REG(CRCDI);
}

static inline
uint16_t crc_readResult(void)
{
    return CRCINIRES;
}


#endif //HAL_CRC_H
