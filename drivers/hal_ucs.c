#include "hal_ucs.h"


ucs_oscillatorSettings_t ucs_config_oscillatorSettings = {
    .xt1_driveMode = UCS_XT1_DRIVEMODE_DEFAULT,
    .xt1_internalCapacitor = UCS_XT1_INTERNALCAPACITOR_DEFAULT,
};
ucs_fllSettings_t ucs_config_fllSettings = {
    .dco_frequencyRange = UCS_DCO_FREQRANGE_DEFAULT,
    .fll_feedbackDivider = UCS_CLK_DIVIDER_2,
    .fll_DCOmultiplier = 0x1f,
    .fll_referenceSource = UCS_FLL_REFERENCE_DEFAULT,
    .fll_referenceDivider = UCS_FLL_REFCLOCK_DIVIDER_DEFAULT,
};


// need to set GPIO for XT1 before calling this
void ucs_setOscillatorConfiguration(ucs_oscillatorSettings_t *oscSettings)
{
    // ensure that highest drive mode is selected for fast and reliable start
    MODIFY_REG(UCSCTL6, __UCS_XT1_DRIVEMODE_BITMASK, XT1DRIVE_3);


    MODIFY_REG(UCSCTL6, __UCS_XT1_INTERNALCAPACITOR_BITMASK, oscSettings->xt1_internalCapacitor);

    // ensure that XT1 has no faults
    while (READ_BIT(UCSCTL7, XT1LFOFFG)) {
        // clear OSC fault flag
        CLEAR_BIT(UCSCTL7, XT1LFOFFG);

        // clear OFIFG fault flag
        CLEAR_BIT(SFRIFG1, OFIFG);
    }

    MODIFY_REG(UCSCTL6, __UCS_XT1_DRIVEMODE_BITMASK, oscSettings->xt1_driveMode);
}

void ucs_setFllConfiguration(ucs_fllSettings_t *fllSettings)
{
//    //Save actual state of FLL loop control, then disable it. This is needed to
//    //prevent the FLL from acting as we are making fundamental modifications to
//    //the clock setup.
//    uint16_t SR_FLL_settings = __get_SR_register() & SCG0;

    // disable FLL
    __bic_SR_register(SCG0);

    MODIFY_REG(UCSCTL3, __UCS_FLL_REFERENCE_BITMASK, fllSettings->fll_referenceSource);

    MODIFY_REG(UCSCTL2, FLLD_7, FLLD0 * fllSettings->fll_feedbackDivider);

    MODIFY_REG(UCSCTL3, FLLREFDIV_7, FLLREFDIV0 * fllSettings->fll_referenceDivider);

    // reset DCO tap and mod bits
    WRITE_REG(UCSCTL0, 0x00);

    WRITE_REG(UCSCTL1, fllSettings->dco_frequencyRange);

    // set multiplier for DCO - 0x03ff is mask because these bits are on positions 9-0
    MODIFY_REG(UCSCTL2, 0x03ff, fllSettings->fll_DCOmultiplier);

//    MODIFY_REG(UCSCTL1, __UCS_DCO_FREQRANGE_BITMASK, fllSettings->dco_frequencyRange);

    // reenable FLL
    __bic_SR_register(SCG0);

    // ensure that DCO has no faults
    while (READ_BIT(UCSCTL7, DCOFFG)) {
        // clear DCO fault flag
        CLEAR_BIT(UCSCTL7, DCOFFG);

        // clear OFIFG fault flag
        CLEAR_BIT(SFRIFG1, OFIFG);
    }

}

void ucs_clockEnable(ucs_clockSettings_t *clkSettings)
{
    switch (clkSettings->clock) {
        case UCS_CLOCK_ACLK:
            MODIFY_REG(UCSCTL4, SELA_7, SELA0 * clkSettings->clkSource);
            MODIFY_REG(UCSCTL5, DIVA_7, DIVA0 * clkSettings->clkSourceDivider);
            break;

        case UCS_CLOCK_MCLK:
            MODIFY_REG(UCSCTL4, SELM_7, SELM0 * clkSettings->clkSource);
            MODIFY_REG(UCSCTL5, DIVM_7, DIVM0 * clkSettings->clkSourceDivider);
            break;

        case UCS_CLOCK_SMCLK:
            MODIFY_REG(UCSCTL4, SELS_7, SELS0 * clkSettings->clkSource);
            MODIFY_REG(UCSCTL5, DIVS_7, DIVS0 * clkSettings->clkSourceDivider);
            break;

        case __UCS_CLOCK_FORCE_SIZE_WORD: break;
    }
}

ucs_clockSettings_t ucs_newDefaultClock(ucs_clockType_t clockType)
{
    ucs_clockSettings_t clk;

    switch (clockType) {
        case UCS_CLOCK_MCLK:
            clk.clock = UCS_CLOCK_MCLK;
            clk.clkSource = UCS_SOURCE_XT1CLK;
            break;

        case UCS_CLOCK_SMCLK:
            clk.clock = UCS_CLOCK_SMCLK;
            clk.clkSource = UCS_SOURCE_DCOCLKDIV;
            break;

        case UCS_CLOCK_ACLK:
            clk.clock = UCS_CLOCK_ACLK;
            clk.clkSource = UCS_SOURCE_DCOCLKDIV;
            break;

        case __UCS_CLOCK_FORCE_SIZE_WORD: break;
    }

    clk.clkSourceDivider = UCS_CLK_DIVIDER_DEFAULT;

    return clk;
}
