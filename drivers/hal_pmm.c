#include "hal_pmm.h"

// default states of both SVSM
pmm_svsm_handler_t pmm_config_svsm_highside = {
    .side = PMM_SVSM_HIGHSIDE,
    .enableSVS = PMM_SVS_ENABLED,
    .enableSVM = PMM_SVM_ENABLED,
    .overvoltageDetection = PMM_SVM_OVERVOLTAGE_DETECTION_DISABLED,
    .automaticControl = PMM_SVSM_AUTOCONTROL_DISABLED,
    .lowPowerModesControl = PMM_SVS_LOWPOWERMODESINTERRUPTS_DISABLED,
    .performanceSVS = PMM_SVS_PERFORMANCE_NORMAL,
    .performanceSVM = PMM_SVM_PERFORMANCE_NORMAL,
    .eventMask = PMM_SVSM_EVENTS_MASKED_NO,
    .delayedEventMask = PMM_SVSM_EVENTS_DELAY_MASKED_NO
};

pmm_svsm_handler_t pmm_config_svsm_lowside = {
    .side = PMM_SVSM_LOWSIDE,
    .enableSVS = PMM_SVS_ENABLED,
    .enableSVM = PMM_SVM_ENABLED,
    .overvoltageDetection = PMM_SVM_OVERVOLTAGE_DETECTION_DISABLED,
    .automaticControl = PMM_SVSM_AUTOCONTROL_DISABLED,
    .lowPowerModesControl = PMM_SVS_LOWPOWERMODESINTERRUPTS_DISABLED,
    .performanceSVS = PMM_SVS_PERFORMANCE_NORMAL,
    .performanceSVM = PMM_SVM_PERFORMANCE_NORMAL,
    .eventMask = PMM_SVSM_EVENTS_MASKED_NO,
    .delayedEventMask = PMM_SVSM_EVENTS_DELAY_MASKED_NO
};




bool pmm_setSVSM(pmm_svsm_handler_t *svsmConfig)
{
    uint16_t setting = svsmConfig->enableSVM | svsmConfig->enableSVS |
                        svsmConfig->eventMask | svsmConfig->performanceSVM |
                        svsmConfig->performanceSVS | svsmConfig->automaticControl |
                        svsmConfig->delayedEventMask | svsmConfig->lowPowerModesControl |
                        svsmConfig->overvoltageDetection;

    uint16_t bitmask =  SVMHFP      |   // performanceSVM
                        SVMHE       |   // enableSVM
                        SVMHOVPE    |   // overvoltageDetection
                        SVSHFP      |   // perormanceSVS
                        SVSHE       |   // enableSVS
                        SVSMHACE    |   // automaticControl
                        SVSMHEVM    |   // eventMask
                        SVSHMD      |   // lowPowerModesControl
                        SVSMHDLYST;     // delayedEventMask

    MODIFY_REG(HWREG16(PMM_BASE + svsmConfig->side), bitmask, setting);

    return true;
}


bool pmm_setVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, pmm_voltageLevels_t newLevel)
{
    bool status = true;
    uint8_t actualLevel = pmm_readVCore();
    uint16_t interruptState = __get_interrupt_state();
    __disable_interrupt();
    __no_operation();

    while ((newLevel != actualLevel) && (status)) {
        if (newLevel > actualLevel) {
            status = pmm_setHigherVCore(highside, lowside, ++actualLevel);
        } else {
            status = pmm_setLowerVCore(highside, lowside, --actualLevel);
        }
    }

    if (interruptState & GIE)
        __enable_interrupt();

    return status;
}

bool pmm_setLowerVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, uint8_t newLevel)
{
    pmm_unlockRegisters();

    uint16_t enabledInterrupts_backup = READ_REG(PMMRIE);

    CLEAR_REG(PMMRIE);
    CLEAR_REG(PMMIFG);

    pmm_highsideSVM_enable();
    pmm_highsideSVS_enable();
    pmm_lowsideSVM_enable();
    pmm_lowsideSVS_enable();

    pmm_setSVSresetVLevel(PMM_SVSM_LOWSIDE, SVSLRVL0 * newLevel);
    pmm_setSVSresetVLevel(PMM_SVSM_HIGHSIDE, SVSHRVL0 * newLevel);
    pmm_setSVSMresetReleaseVLevel(PMM_SVSM_LOWSIDE, SVSMLRRL0 * newLevel);
    pmm_setSVSMresetReleaseVLevel(PMM_SVSM_HIGHSIDE, SVSMHRRL0 * newLevel);

    while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0 ||
           READ_BIT(PMMIFG, SVSMLDLYIFG) == 0);

    CLEAR_BIT(PMMIFG, SVSMHDLYIFG | SVSMLDLYIFG);

    SET_BIT(PMMCTL0, PMMCOREV0 * newLevel);

    pmm_setSVSM(highside);
    pmm_setSVSM(lowside);

    while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0 ||
           READ_BIT(PMMIFG, SVSMLDLYIFG) == 0);

    CLEAR_REG(PMMIFG);
    WRITE_REG(PMMRIE, enabledInterrupts_backup);

    pmm_lockRegisters();

    return true;
}

bool pmm_setHigherVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, uint8_t newLevel)
{
    pmm_unlockRegisters();

    uint16_t enabledInterrupts_backup = READ_REG(PMMRIE);

    CLEAR_REG(PMMRIE);
    CLEAR_REG(PMMIFG);

    pmm_highsideSVM_enable();
    pmm_highsideSVS_enable();

    pmm_setSVSMresetReleaseVLevel(PMM_SVSM_HIGHSIDE, SVSMHRRL0 * newLevel);

    while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0);

    CLEAR_BIT(PMMIFG, SVSMHDLYIFG);

    if (READ_BIT(PMMIFG, SVMHIFG)) {
        // if condition is true - Vcc is too low to increase VCore
        CLEAR_BIT(PMMIFG, SVSMHDLYIFG);
        pmm_setSVSM(highside);

        while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0);

        CLEAR_REG(PMMIFG);

        WRITE_REG(PMMRIE, enabledInterrupts_backup);

        pmm_lockRegisters();

        return false;
    }

    pmm_setSVSresetVLevel(PMM_SVSM_HIGHSIDE, SVSHRVL0 * newLevel);

    while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0);

    CLEAR_BIT(PMMIFG, SVSMHDLYIFG);

    uint8_t nl = PMMCOREV0 * newLevel;

//    SET_BIT(PMMCTL0, PMMCOREV0 * newLevel);
    SET_BIT(PMMCTL0_L, nl);

    pmm_lowsideSVM_enable();
    pmm_lowsideSVS_enable();

    pmm_setSVSMresetReleaseVLevel(PMM_SVSM_LOWSIDE, SVSMLRRL0 * newLevel);
    pmm_setSVSresetVLevel(PMM_SVSM_LOWSIDE, SVSLRVL0 * newLevel);

    while (READ_BIT(PMMIFG, SVSMLDLYIFG) == 0);

    CLEAR_BIT(PMMIFG, SVSMLDLYIFG);

    pmm_setSVSM(highside);
    pmm_setSVSM(lowside);

    while (READ_BIT(PMMIFG, SVSMHDLYIFG) == 0 ||
           READ_BIT(PMMIFG, SVSMLDLYIFG) == 0);

    CLEAR_REG(PMMIFG);
    WRITE_REG(PMMRIE, enabledInterrupts_backup);

    pmm_lockRegisters();

    return true;
}
