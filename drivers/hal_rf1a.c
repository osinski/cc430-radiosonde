/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 CC1101 Radio Based Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/


#include "hal_rf1a.h"


void rf1a_setPacketModeSettings(rf1a_packetMode_t *packetSettings)
{
    rf1a_writeSingleReg(ADDR, packetSettings->deviceAddress);

    rf1a_modifySingleReg(FIFOTHR, RF1A_FIFOTHRESHOLD_BITMASK,
                         packetSettings->fifoThreshold);

    rf1a_writeSingleReg(PKTLEN, packetSettings->packetLength);

    rf1a_modifySingleReg(PKTCTRL0, RF1A_PACKETLENGTH_BITMASK,
                         packetSettings->packetLengthMode);

    rf1a_modifySingleReg(MDMCFG4, 0x0f, packetSettings->symbolRate_exponent & 0x0f);
    rf1a_writeSingleReg(MDMCFG3, packetSettings->symbolRate_mantissa);

    rf1a_modifySingleReg(MDMCFG2, RF1A_SYNCMODE_BITMASK, packetSettings->syncMode);

    rf1a_writeSingleReg(SYNC1, (packetSettings->syncWord & 0xff00) >> 8);
    rf1a_writeSingleReg(SYNC0, packetSettings->syncWord & 0x00ff);

    rf1a_modifySingleReg(MCSM1, RF1A_OFFMODE_TX_BITMASK, packetSettings->txOffMode);
    rf1a_modifySingleReg(MCSM1, RF1A_OFFMODE_RX_BITMASK, packetSettings->rxOffMode);

}

void rf1a_setGdoSettings(rf1a_gdox_t *gdoSettings)
{
    rf1a_writeSingleReg(IOCFG0, gdoSettings->gdo0Config);
    rf1a_writeSingleReg(IOCFG1, gdoSettings->gdo1Config);
    rf1a_writeSingleReg(IOCFG2, gdoSettings->gdo2Config);
}

void rf1a_setTxSettings(rf1a_txSettings_t *txSettings)
{
    rf1a_modifySingleReg(FREND0, 0x03, txSettings->powerAmplifierTableIndex & 0x03);
}

void rf1a_setCommonSettings(rf1a_commonSettings_t *radio)
{
    rf1a_modifySingleReg(PKTCTRL0, RF1A_PACKETFORMAT_BITMASK,
                         radio->packetFormat & 0x03);

    rf1a_writeSingleReg(FREQ2, (radio->carrierFrequency & 0x003f0000) >> 16);
    rf1a_writeSingleReg(FREQ1, (radio->carrierFrequency & 0x0000ff00) >> 8);
    rf1a_writeSingleReg(FREQ0, radio->carrierFrequency & 0x000000ff);

    rf1a_modifySingleReg(MDMCFG2, RF1A_MODULATIONFORMAT_BITMASK,
                         radio->modulationFormat);

    rf1a_modifySingleReg(MCSM0, RF1a_FSAUTOCAL_BITMASK, radio->fsAutoCalibration);
    rf1a_modifySingleReg(DEVIATN, 0x70, radio->deviation_exponent & 0x07);
    rf1a_modifySingleReg(DEVIATN, 0x03, radio->deviation_mantissa & 0x03);
}


void rf1a_writeRfSettings(rf1a_settings_t *pRfSettings) {
    rf1a_writeSingleReg(IOCFG2,     pRfSettings->iocfg2);
    rf1a_writeSingleReg(IOCFG1,     pRfSettings->iocfg1);
    rf1a_writeSingleReg(IOCFG0,     pRfSettings->iocfg0);
    rf1a_writeSingleReg(FIFOTHR,    pRfSettings->fifothr);
    rf1a_writeSingleReg(SYNC1,      pRfSettings->sync1);
    rf1a_writeSingleReg(SYNC0,      pRfSettings->sync0);
    rf1a_writeSingleReg(PKTLEN,     pRfSettings->pktlen);
    rf1a_writeSingleReg(PKTCTRL1,   pRfSettings->pktctrl1);
    rf1a_writeSingleReg(PKTCTRL0,   pRfSettings->pktctrl0);
    rf1a_writeSingleReg(ADDR,       pRfSettings->addr);
    rf1a_writeSingleReg(CHANNR,     pRfSettings->channr);
    rf1a_writeSingleReg(FSCTRL1,    pRfSettings->fsctrl1);
    rf1a_writeSingleReg(FSCTRL0,    pRfSettings->fsctrl0);
    rf1a_writeSingleReg(FREQ2,      pRfSettings->freq2);
    rf1a_writeSingleReg(FREQ1,      pRfSettings->freq1);
    rf1a_writeSingleReg(FREQ0,      pRfSettings->freq0);
    rf1a_writeSingleReg(MDMCFG4,    pRfSettings->mdmcfg4);
    rf1a_writeSingleReg(MDMCFG3,    pRfSettings->mdmcfg3);
    rf1a_writeSingleReg(MDMCFG2,    pRfSettings->mdmcfg2);
    rf1a_writeSingleReg(MDMCFG1,    pRfSettings->mdmcfg1);
    rf1a_writeSingleReg(MDMCFG0,    pRfSettings->mdmcfg0);
    rf1a_writeSingleReg(DEVIATN,    pRfSettings->deviatn);
    rf1a_writeSingleReg(MCSM2,      pRfSettings->mcsm2);
    rf1a_writeSingleReg(MCSM1,      pRfSettings->mcsm1);
    rf1a_writeSingleReg(MCSM0 ,     pRfSettings->mcsm0);
    rf1a_writeSingleReg(FOCCFG,     pRfSettings->foccfg);
    rf1a_writeSingleReg(BSCFG,      pRfSettings->bscfg);
    rf1a_writeSingleReg(AGCCTRL2,   pRfSettings->agcctrl2);
    rf1a_writeSingleReg(AGCCTRL1,   pRfSettings->agcctrl1);
    rf1a_writeSingleReg(AGCCTRL0,   pRfSettings->agcctrl0);
    rf1a_writeSingleReg(WOREVT1,    pRfSettings->worevt1);
    rf1a_writeSingleReg(WOREVT0,    pRfSettings->worevt0);
    rf1a_writeSingleReg(WORCTRL,    pRfSettings->worctrl);
    rf1a_writeSingleReg(FREND1,     pRfSettings->frend1);
    rf1a_writeSingleReg(FREND0,     pRfSettings->frend0);
    rf1a_writeSingleReg(FSCAL3,     pRfSettings->fscal3);
    rf1a_writeSingleReg(FSCAL2,     pRfSettings->fscal2);
    rf1a_writeSingleReg(FSCAL1,     pRfSettings->fscal1);
    rf1a_writeSingleReg(FSCAL0,     pRfSettings->fscal0);
    rf1a_writeSingleReg(FSTEST,     pRfSettings->fstest);
    rf1a_writeSingleReg(PTEST,      pRfSettings->ptest);
    rf1a_writeSingleReg(AGCTEST,    pRfSettings->agctest);
    rf1a_writeSingleReg(TEST2,      pRfSettings->test2);
    rf1a_writeSingleReg(TEST1,      pRfSettings->test1);
    rf1a_writeSingleReg(TEST0,      pRfSettings->test0);
}


bool rf1a_changeMachineState(rf1a_machineStates_t newState)
{
    // current state is also in status register and it may be more elegant to
    // read it from there
    uint8_t currentState = rf1a_readMARCSTATE();

    switch (newState)
    {
    case RF1A_STATE_SLEEP:
        if (currentState == 1)              // 1 is IDLE
        {
            rf1a_strobe(RF_SXOFF);
            return true;
        }
        else
            return false;

    case RF1A_STATE_IDLE:
        rf1a_strobe(RF_SIDLE);              // you can go to IDLE from anywhere
        return true;

    case RF1A_STATE_FS_MANCAL:
        if (currentState == 1)              // 1 is IDLE
        {
            rf1a_strobe(RF_SCAL);
            return true;
        }
        else
            return false;


    case RF1A_STATE_FS_TXON:
        if (currentState <= 1)              // 1 is IDLE, 0 is SLEEP
        {
            rf1a_strobe(RF_SFSTXON);
            return true;
        }
        else
            return false;

    case RF1A_STATE_TX:
        if (currentState <= 1 || currentState == 18)    // 18 is FSTXON
        {
            rf1a_strobe(RF_STX);
            return true;
        }
        else
            return false;

    case RF1A_STATE_RX:
        if (currentState <= 1 || currentState == 18)
        {
            rf1a_strobe(RF_SRX);
            return true;
        }
        else
            return false;
    }

    return false;
}

// *************************************************************************************************
// @fn          Strobe
// @brief       Send command to radio.
// @param       uint8_t strobe    Command to radio
// @return      statusByte                              Radio core status
// *************************************************************************************************
uint8_t rf1a_strobe(uint8_t strobe)
{
    uint8_t statusByte = 0;
    uint16_t int_state, gdo_state;

    // Check for valid strobe command
    if ((strobe == 0xBD) || ((strobe > RF_SRES) && (strobe < RF_SNOP)))
    {
        ENTER_CRITICAL_SECTION(int_state);

        // Clear the Status read flag
        RF1AIFCTL1 &= ~(RFSTATIFG);

        // Wait for radio to be ready for next instruction
        while (!(RF1AIFCTL1 & RFINSTRIFG)) ;

        // Write the strobe instruction
        if ((strobe > RF_SRES) && (strobe < RF_SNOP))
        {

            gdo_state = rf1a_readSingleReg(IOCFG2);         // buffer IOCFG2 state
            rf1a_writeSingleReg(IOCFG2, 0x29);              // c-ready to GDO2

            RF1AINSTRB = strobe;
            if ((RF1AIN & 0x04) == 0x04)               // chip at sleep mode
            {
                if ((strobe == RF_SXOFF) || (strobe == RF_SPWD) || (strobe == RF_SWOR))
                {
                }
                else
                {
                    while ((RF1AIN & 0x04) == 0x04) ;  // c-ready ?
//                    __delay_cycles(9800);              // Delay for ~810usec at 12MHz CPU clock
                }
            }
            rf1a_writeSingleReg(IOCFG2, gdo_state);         // restore IOCFG2 setting
        }
        else                                           // chip active mode
        {
            RF1AINSTRB = strobe;
        }
        statusByte = RF1ASTATB;
        while (!(RF1AIFCTL1 & RFSTATIFG)) ;
        EXIT_CRITICAL_SECTION(int_state);
    }
    return statusByte;
}

// *************************************************************************************************
// @fn          ResetRadioCore
// @brief       Software reset radio core.
// @param       none
// @return      none
// *************************************************************************************************
void rf1a_resetRadioCore(void)
{
    rf1a_strobe(RF_SRES);            // Reset the Radio Core
    rf1a_strobe(RF_SNOP);            // Reset Radio Pointer
}

// *************************************************************************************************
// @fn          rf1a_readSingleReg
// @brief       Read byte from register.
// @param       none
// @return      none
// *************************************************************************************************
uint8_t rf1a_readSingleReg(uint8_t addr)
{
    uint8_t x;
    uint16_t int_state;

    ENTER_CRITICAL_SECTION(int_state);

    RF1AINSTR1B = (addr | RF_REGRD);
    x = RF1ADOUT1B;

    EXIT_CRITICAL_SECTION(int_state);

    return x;
}

// *************************************************************************************************
// @fn          rf1a_writeSingleReg
// @brief       Write byte to register.
// @param       none
// @return      none
// *************************************************************************************************
void rf1a_writeSingleReg(uint8_t addr, uint8_t value)
{
    volatile unsigned int i;
    uint16_t int_state;

    ENTER_CRITICAL_SECTION(int_state);

    while (!(RF1AIFCTL1 & RFINSTRIFG)) ;           // Wait for the Radio to be ready for the next
                                                   // instruction

    RF1AINSTRW = ((addr | RF_REGWR) << 8) + value; // Send address + Instruction
    while (!(RFDINIFG & RF1AIFCTL1)) ;

    i = RF1ADOUTB;                                 // Reset RFDOUTIFG flag which contains status
                                                   // byte

    EXIT_CRITICAL_SECTION(int_state);
}

// *************************************************************************************************
// @fn          ReadBurstReg
// @brief       Read sequence of bytes from register.
// @param       none
// @return      none
// *************************************************************************************************
void rf1a_readBurstReg(uint8_t addr, uint8_t *buffer, uint8_t count)
{
    unsigned int i;
    uint16_t int_state;

    ENTER_CRITICAL_SECTION(int_state);

    while (!(RF1AIFCTL1 & RFINSTRIFG)) ;     // Wait for the Radio to be ready for next instruction
    RF1AINSTR1B = (addr | RF_REGRD);         // Send address + Instruction

    for (i = 0; i < (count - 1); i++)
    {
        while (!(RFDOUTIFG & RF1AIFCTL1)) ;  // Wait for the Radio Core to update the RF1ADOUTB reg
        buffer[i] = RF1ADOUT1B;              // Read DOUT from Radio Core + clears RFDOUTIFG
        // Also initiates auo-read for next DOUT byte
    }
    buffer[count - 1] = RF1ADOUT0B;          // Store the last DOUT from Radio Core

    EXIT_CRITICAL_SECTION(int_state);
}

// *************************************************************************************************
// @fn          WriteBurstReg
// @brief       Write sequence of bytes to register.
// @param       none
// @return      none
// *************************************************************************************************
void rf1a_writeBurstReg(uint8_t addr, uint8_t *buffer, uint8_t count)
{
    // Write Burst works wordwise not bytewise - bug known already
    uint8_t i;
    uint16_t int_state;

    ENTER_CRITICAL_SECTION(int_state);

    while (!(RF1AIFCTL1 & RFINSTRIFG)) ;               // Wait for the Radio to be ready for next
                                                       // instruction
    RF1AINSTRW = ((addr | RF_REGWR) << 8) + buffer[0]; // Send address + Instruction

    for (i = 1; i < count; i++)
    {
        RF1ADINB = buffer[i];                          // Send data
        while (!(RFDINIFG & RF1AIFCTL1)) ;             // Wait for TX to finish
    }
    i = RF1ADOUTB;                                     // Reset RFDOUTIFG flag which contains status
                                                       // byte

    EXIT_CRITICAL_SECTION(int_state);
}

void rf1a_modifySingleReg(uint8_t addr, uint8_t clrmask, uint8_t setmask)
{
    uint8_t reg = rf1a_readSingleReg(addr);

    MODIFY_REG(reg, clrmask, setmask);

    rf1a_writeSingleReg(addr, reg);
}

// *************************************************************************************************
// @fn          WritePATable
// @brief       Write data to power table
// @param       uint8_t value             Value to write
// @return      none
// *************************************************************************************************
void rf1a_writePATable(uint8_t value)
{
    uint8_t readbackPATableValue = 0;
    uint16_t int_state;

    ENTER_CRITICAL_SECTION(int_state);

    while (readbackPATableValue != value)
    {
        while (!(RF1AIFCTL1 & RFINSTRIFG)) ;
        RF1AINSTRW = 0x7E00 + value; // PA Table write (burst)

        while (!(RF1AIFCTL1 & RFINSTRIFG)) ;
        RF1AINSTRB = RF_SNOP;        // reset pointer

        while (!(RF1AIFCTL1 & RFINSTRIFG)) ;
        RF1AINSTRB = 0xFE;           // PA Table read (burst)

        while (!(RF1AIFCTL1 & RFDINIFG)) ;
        RF1ADINB = 0x00;             //dummy write

        while (!(RF1AIFCTL1 & RFDOUTIFG)) ;
        readbackPATableValue = RF1ADOUT0B;

        while (!(RF1AIFCTL1 & RFINSTRIFG)) ;
        RF1AINSTRB = RF_SNOP;
    }

    EXIT_CRITICAL_SECTION(int_state);
}

void rf1a_writeSinglePATable(uint8_t value)
{
    while(!(RF1AIFCTL1 & RFINSTRIFG));
    RF1AINSTRW = 0x3E00 + value;              // PA Table single write

    while(!(RF1AIFCTL1 & RFINSTRIFG));
    RF1AINSTRB = RF_SNOP;                     // reset PA_Table pointer
}

