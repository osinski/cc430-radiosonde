/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 GPIO PORTS Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_GPIO_H
#define HAL_GPIO_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"

#include "hal_pmap.h"


typedef struct
{
    gpio_ports_t port;
    gpio_pins_t pin;
    gpio_directions_t dir;

    gpio_interrupts_t interrupt;
    gpio_interruptEdges_t interruptEdge;

    gpio_pullResistors_t pullResistor;

    gpio_driveStrengths_t driveStrength;

    gpio_functions_t function;

    pmap_mappings_t mapping;

} gpio_handler_t;

void gpio_configure(gpio_handler_t *gpio);

gpio_handler_t gpio_newDefaultGPIO(gpio_ports_t port, gpio_pins_t pin);

void gpio_writePin(gpio_ports_t port, gpio_pins_t pin, gpio_states_t state);
void gpio_setPinHigh(gpio_ports_t port, gpio_pins_t pin);
void gpio_setPinLow(gpio_ports_t port, gpio_pins_t pin);
void gpio_togglePin(gpio_ports_t port, gpio_pins_t pin);
uint8_t gpio_readPin(gpio_ports_t port, gpio_pins_t pin);


static inline void gpio_write(gpio_handler_t *gpio, gpio_states_t state)
{
    gpio->dir == GPIO_OUTPUT ? gpio_writePin(gpio->port, gpio->pin, state) : (void)0;
}

static inline void gpio_setHigh(gpio_handler_t *gpio)
{
    gpio->dir == GPIO_OUTPUT ? gpio_setPinHigh(gpio->port, gpio->pin) : (void)0;
}

static inline void gpio_setLow(gpio_handler_t *gpio)
{
    gpio->dir == GPIO_OUTPUT ? gpio_setPinLow(gpio->port, gpio->pin) : (void)0;
}

static inline void gpio_toggle(gpio_handler_t *gpio)
{
    gpio->dir == GPIO_OUTPUT ? gpio_togglePin(gpio->port, gpio->pin) : (void)0;
}

static inline uint8_t gpio_read(gpio_handler_t *gpio)
{
    return (gpio->dir == GPIO_INPUT ? gpio_readPin(gpio->port, gpio->pin) : 0);
}

static inline void gpio_setAsDefaultPeripheral_output(gpio_ports_t port, gpio_pins_t pin)
{
    SET_BIT(HWREG8(port + DIRECTION_REGISTER), 1 << pin);
    SET_BIT(HWREG8(port + SELECTION_REGISTER), 1 << pin);
}

static inline void gpio_setAsDefaultPeripheral_input(gpio_ports_t port, gpio_pins_t pin)
{
    CLEAR_BIT(HWREG8(port + DIRECTION_REGISTER), 1 << pin);
    SET_BIT(HWREG8(port + SELECTION_REGISTER), 1 << pin);
}

#endif  //HAL_GPIO_H
