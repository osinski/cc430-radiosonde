/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Universal Serial Control Interface B Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef USCI_I2C_H
#define USCI_I2C_H

#include <cc430f5137.h>
#include <stdint.h>

#include "hal_common_defs.h"


typedef struct
{
    usci_i2c_modes_t mode;
    usci_i2c_environments_t environment;
    usci_i2c_ownAddressModes_t ownAddressMode;
    usci_i2c_slaveAddressModes_t slaveAddressMode;

    uint16_t ownAddress;

} usci_i2c_handler_t;

void usci_i2c_init(usci_i2c_handler_t *i2c);

void usci_i2c_master_transmitByte_blocking(usci_i2c_handler_t *i2c, uint8_t byte);
uint8_t usci_i2c_master_receiveByte_blocking(usci_i2c_handler_t *i2c);

void usci_i2c_slave_transmitByte_blocking(usci_i2c_handler_t *i2c, uint8_t byte);
uint8_t usci_i2c_slave_receiveByte_blocking(usci_i2c_handler_t *i2c);


static inline
void usci_i2c_setSlaveAddress(uint16_t addr)
{
    // truncate it to 9bits (length of the target bitfield)
    addr &= 0x1ff;

    MODIFY_REG(UCB0I2CSA, 0x1ff, addr);
}

static inline
void usci_i2c_setAsReceiver(void)
{
    CLEAR_BIT(UCB0CTL1, UCTR);
}

static inline
void usci_i2c_setAsTransmitter(void)
{
    SET_BIT(UCB0CTL1, UCTR);
}

static inline
void usci_i2c_sendStartCondition(void)
{
    SET_BIT(UCB0CTL1, UCTXSTT);
}

static inline
bool usci_i2c_isStartConditionSent(void)
{
    return (bool) READ_BIT(UCB0CTL1, UCTXSTT);
}

static inline
void usci_i2c_sendStopCondition(void)
{
    SET_BIT(UCB0CTL1, UCTXSTP);
}

static inline
bool usci_i2c_isStopConditionSent(void)
{
    return (bool) READ_BIT(UCB0CTL1, UCTXSTP);
}

static inline
void usci_i2c_sendNACK(void)
{
    SET_BIT(UCB0CTL1, UCTXNACK);
}

static inline
bool usci_i2c_isNACKsent(void)
{
    return (bool) READ_BIT(UCB0CTL1, UCTXNACK);
}

static inline
void usci_i2c_enableSpecialInterrupt(usci_i2c_specialInterrupts_t specInterrupt)
{
    SET_BIT(UCB0IE, specInterrupt);
}

static inline
void usci_i2c_disableSpecialInterrupt(usci_i2c_specialInterrupts_t specInterrupt)
{
    CLEAR_BIT(UCB0IE, specInterrupt);
}

static inline
void usci_i2c_clearSpecialInterruptFlag(usci_i2c_specialInterrupts_t specInterrupt)
{
    CLEAR_BIT(UCB0IFG, specInterrupt);
}



#endif
