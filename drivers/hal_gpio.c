#include "hal_gpio.h"


void gpio_configure(gpio_handler_t *gpio)
{
    if (gpio->interrupt == GPIO_INTERRUPT_ENABLE)
            SET_BIT(HWREG8(gpio->port + INTERRUPT_ENABLE_REGISTER), 1<<gpio->pin);

    else if (gpio->interrupt == GPIO_INTERRUPT_DISABLE)
            CLEAR_BIT(HWREG8(gpio->port + INTERRUPT_ENABLE_REGISTER), 1<<gpio->pin);


    if (gpio->interruptEdge == GPIO_INTERRUPT_EDGE_FALLING)
            SET_BIT(HWREG8(gpio->port + INTERRUPT_EDGESELECT_REGISTER), 1<<gpio->pin);

    else if (gpio->interruptEdge == GPIO_INTERRUPT_EDGE_RISING)
            CLEAR_BIT(HWREG8(gpio->port + INTERRUPT_EDGESELECT_REGISTER), 1<<gpio->pin);


    if (gpio->dir == GPIO_INPUT)
    {
        CLEAR_BIT(HWREG8(gpio->port + DIRECTION_REGISTER), 1<<gpio->pin);

        if (gpio->pullResistor == GPIO_PULL_UP || gpio->pullResistor == GPIO_PULL_DOWN)
        {
            SET_BIT(HWREG8(gpio->port + PULLRESISTORS_REGISTER), 1<<gpio->pin);

            if (gpio->pullResistor == GPIO_PULL_UP)
                SET_BIT(HWREG8(gpio->port + OUTPUT_REGISTER), 1<<gpio->pin);
            else if (gpio->pullResistor == GPIO_PULL_DOWN)
                CLEAR_BIT(HWREG8(gpio->port + OUTPUT_REGISTER), 1<<gpio->pin);
        }
    }
    else if (gpio->dir == GPIO_OUTPUT)
        SET_BIT(HWREG8(gpio->port + DIRECTION_REGISTER), 1<<gpio->pin);


    if (gpio->driveStrength == GPIO_DRIVESTRENGTH_FULL)
        SET_BIT(HWREG8(gpio->port + DRIVESTRENGTH_REGISTER), 1<<gpio->pin);
    else if (gpio->driveStrength == GPIO_DRIVESTRENGTH_REDUCED)
        CLEAR_BIT(HWREG8(gpio->port + DRIVESTRENGTH_REGISTER), 1<<gpio->pin);


    if (gpio->function == GPIO_FUNCTION_PERIPHERAL)
    {
        uint16_t portOffset = 0;

        switch (gpio->port)
        {
            case GPIO_PORT_1:
                portOffset = P1MAP_BASE - P1MAP_BASE;
                break;

            case GPIO_PORT_2:
                portOffset = P2MAP_BASE - P1MAP_BASE;
                break;

            case GPIO_PORT_3:
                portOffset = P3MAP_BASE - P1MAP_BASE;
                break;

            case GPIO_PORT_5:
                break;
        }

        pmap_unlockRegisters();
        WRITE_REG(HWREG8(P1MAP_BASE + portOffset + gpio->pin), gpio->mapping);
        pmap_lockRegisters();

        SET_BIT(HWREG8(gpio->port + SELECTION_REGISTER), 1<<gpio->pin);
    }
    else if (gpio->function == GPIO_FUNCTION_IO)
        CLEAR_BIT(HWREG8(gpio->port + SELECTION_REGISTER), 1<<gpio->pin);

}

gpio_handler_t gpio_newDefaultGPIO(gpio_ports_t port, gpio_pins_t pin)
{
    gpio_handler_t gpio;

    gpio.port = port;
    gpio.pin = pin;

    gpio.dir = GPIO_INPUT;
    gpio.function = GPIO_FUNCTION_IO;
    gpio.interrupt = GPIO_INTERRUPT_DISABLE;
    gpio.mapping = PMAP_NONE;
    gpio.pullResistor = GPIO_PULL_NONE;
    gpio.driveStrength = GPIO_DRIVESTRENGTH_REDUCED;
    gpio.interruptEdge = GPIO_INTERRUPT_EDGE_RISING;

    return gpio;
}

void gpio_writePin(gpio_ports_t port, gpio_pins_t pin, gpio_states_t state)
{
    if (state == GPIO_LOW)
        gpio_setPinLow(port, pin);
    else if (state == GPIO_HIGH)
        gpio_setPinHigh(port, pin);
}

void gpio_setPinHigh(gpio_ports_t port, gpio_pins_t pin)
{
//    if (port == GPIO_PORT_3)
//        SET_BIT(GPIO_PortB[OUTPUT_REGISTER], pin);

//    else
//        SET_BIT(GPIO_PortA[OUTPUT_REGISTER], port == GPIO_PORT_2 ?
//                                             pin << 8 : pin);

    SET_BIT(HWREG8(port + OUTPUT_REGISTER), 1<<pin);
}

void gpio_setPinLow(gpio_ports_t port, gpio_pins_t pin)
{
//    if (port == GPIO_PORT_3)
//        CLEAR_BIT(GPIO_PortB[OUTPUT_REGISTER], pin);

//    else
//        CLEAR_BIT(GPIO_PortA[OUTPUT_REGISTER], port == GPIO_PORT_2 ?
//                                               pin << 8 : pin);

    CLEAR_BIT(HWREG8(port + OUTPUT_REGISTER), 1<<pin);
}

void gpio_togglePin(gpio_ports_t port, gpio_pins_t pin)
{
//    if (port == GPIO_PORT_3)
//        TOGGLE_BIT(GPIO_PortB[OUTPUT_REGISTER], pin);

//    else
//        TOGGLE_BIT(GPIO_PortA[OUTPUT_REGISTER], port == GPIO_PORT_2 ?
//                                                pin << 8 : pin);

    TOGGLE_BIT(HWREG8(port + OUTPUT_REGISTER), 1<<pin);
}

uint8_t gpio_readPin(gpio_ports_t port, gpio_pins_t pin)
{
//    if (port == GPIO_PORT_3)
//        return READ_BIT(GPIO_PortB[INPUT_REGISTER], pin);

//    else
//        return READ_BIT(GPIO_PortA[INPUT_REGISTER], port == GPIO_PORT_2 ?
//                                                    pin << 8 : pin);

    return READ_BIT(HWREG8(port + INPUT_REGISTER), 1<<pin);
}
