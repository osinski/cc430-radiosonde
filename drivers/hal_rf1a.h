/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 CC1101 Radio Based Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_RF1A_H
#define HAL_RF1A_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"


typedef struct
{
    rf1a_fifoTresholds_t fifoThreshold;
    uint8_t packetLength;
    rf1a_packetLengthsModes_t packetLengthMode;
    uint8_t deviceAddress;
    rf1a_preambleLengths_t preambleLength;
    rf1a_syncModes_t syncMode;
    rf1a_packetOffModes_t txOffMode;
    rf1a_packetOffModes_t rxOffMode;

    uint8_t symbolRate_mantissa;
    uint8_t symbolRate_exponent;

    uint16_t syncWord;
} rf1a_packetMode_t;

void rf1a_setPacketModeSettings(rf1a_packetMode_t *packetSettings);

typedef struct
{
    rf1a_gdoxModes_t gdo2Config;
    rf1a_gdoxModes_t gdo1Config;
    rf1a_gdoxModes_t gdo0Config;
} rf1a_gdox_t;

void rf1a_setGdoSettings(rf1a_gdox_t *gdoSettings);

typedef struct
{
    uint8_t powerAmplifierTableIndex;

} rf1a_txSettings_t;

void rf1a_setTxSettings(rf1a_txSettings_t *txSettings);

typedef struct
{
    rf1a_packetFormats_t packetFormat;   // sets operating mode (part of Packet Automation Control)

    rf1a_modulationFormats_t modulationFormat;
    uint8_t deviation_mantissa;
    uint8_t deviation_exponent;

    rf1a_fsAutocalibrationModes_t fsAutoCalibration;

    uint32_t carrierFrequency;
} rf1a_commonSettings_t;

void rf1a_setCommonSettings(rf1a_commonSettings_t *radio);


void rf1a_writeRfSettings(rf1a_settings_t *pRfSettings);

uint8_t rf1a_strobe(uint8_t strobe);
uint8_t rf1a_readSingleReg(uint8_t addr);
void rf1a_writeSingleReg(uint8_t addr, uint8_t value);
void rf1a_readBurstReg(uint8_t addr, uint8_t *buffer, uint8_t count);
void rf1a_writeBurstReg(uint8_t addr, uint8_t *buffer, uint8_t count);
void rf1a_modifySingleReg(uint8_t addr, uint8_t clrmask, uint8_t setmask);
void rf1a_resetRadioCore(void);
void rf1a_writeSinglePATable(uint8_t value);
void rf1a_writePATable(uint8_t value);
void rf1a_waitForXT2(void);

bool rf1a_changeMachineState(rf1a_machineStates_t newState);

static inline uint8_t rf1a_readStatusReg(void)
{
    return rf1a_strobe(RF_SNOP);
}

static inline uint8_t rf1a_readPartNumber(void)
{
    return rf1a_readSingleReg(PARTNUM);
}

static inline uint8_t rf1a_readVersion(void)
{
    return rf1a_readSingleReg(VERSION);
}

static inline uint8_t rf1a_readFrequencyEstimation(void)
{
    return rf1a_readSingleReg(FREQEST);
}

static inline uint8_t rf1a_readLQI(void)
{
    return rf1a_readSingleReg(LQI);
}

static inline uint8_t rf1a_readRSSI(void)
{
    return rf1a_readSingleReg(RSSI);
}

static inline uint8_t rf1a_readMARCSTATE(void)
{
    return rf1a_readSingleReg(MARCSTATE);
}

static inline uint16_t rf1a_readWORtime(void)
{
    return ((rf1a_readSingleReg(WORTIME1) << 8) | rf1a_readSingleReg(WORTIME0));
}

static inline uint8_t rf1a_readPacketStatus(void)
{
    return rf1a_readSingleReg(PKTSTATUS);
}

static inline uint8_t rf1a_readPLLcalibration(void)
{
    return rf1a_readSingleReg(VCO_VC_DAC);
}

static inline uint8_t rf1a_readTxFifoBytesNumber(void)
{
    return rf1a_readSingleReg(TXBYTES);
}

static inline uint8_t rf1a_readRxFifoBytesNumber(void)
{
    return rf1a_readSingleReg(RXBYTES);
}

static inline void rf1a_enableCoreInterrupts(uint16_t interrupts)
{
    MODIFY_REG(RF1AIE, interrupts, interrupts);
}

static inline void rf1a_enableInterfaceInterrupts(uint16_t interrupts)
{
    MODIFY_REG(RF1AIFCTL1, interrupts & 0xff00, interrupts & 0xff00);
}

static inline void rf1a_disableCoreInterrupts(uint16_t interrupts)
{
    MODIFY_REG(RF1AIE, interrupts, 0);
}

static inline void rf1a_disableInterfaceInterrupts(uint16_t interrupts)
{
    MODIFY_REG(RF1AIFCTL1, interrupts & 0xff00, 0);
}

static inline bool rf1a_isCoreInterruptPending(uint16_t interrupt)
{
    return READ_BIT(RF1AIFG, interrupt);
}

static inline bool rf1a_isInterfaceInterruptPending(uint16_t interrupt)
{
    return READ_BIT(RF1AIFCTL1, interrupt & 0x00ff);
}

static inline bool rf1a_isInterfaceErrorInterruptPending(uint16_t interrupt)
{
    return READ_BIT(RF1AIFERR, interrupt);
}

static inline void rf1a_setCoreInterruptEdge(uint16_t edgeSelect)
{
    WRITE_REG(RF1AIES, edgeSelect);
}

static inline uint16_t rf1a_readCoreInterruptEdge(void)
{
    return READ_REG(RF1AIES);
}


#endif //HAL_RF1A_H
