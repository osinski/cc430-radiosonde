#include "hal_usci_i2c.h"
#include "hal_usci_common.h"


usci_i2c_handler_t usci_b_i2c_handler = {
    .mode = USCI_I2C_MODE_SLAVE,
    .environment = USCI_I2C_ENVIRONMENT_SINGLEMASTER,
    .ownAddressMode = USCI_I2C_OWN_ADDRESS_7BIT,
    .slaveAddressMode = USCI_I2C_SLAVE_ADDRESS_7BIT,

    .ownAddress = 0xaa
};


void usci_i2c_init(usci_i2c_handler_t *i2c)
{
    WRITE_REG(UCB0CTL0, i2c->mode | i2c->ownAddressMode | i2c->slaveAddressMode |
              i2c->environment | UCMODE_3 | UCSYNC);

    MODIFY_REG(UCB0I2COA, 0x1ff, i2c->ownAddress & 0x1ff);
}

void usci_i2c_master_transmitByte_blocking(usci_i2c_handler_t *i2c, uint8_t byte)
{
    while (!(usci_readInterruptFlags(USCI_B_0) & USCI_INTERRUPTS_TX));

    usci_writeTxBuf(USCI_B_0, byte);
}

uint8_t usci_i2c_master_receiveByte_blocking(usci_i2c_handler_t *i2c)
{
    while (!(usci_readInterruptFlags(USCI_B_0) & USCI_INTERRUPTS_RX));

    return usci_readRxBuf(USCI_B_0);
}

void usci_i2c_slave_transmitByte_blocking(usci_i2c_handler_t *i2c, uint8_t byte)
{
    usci_writeTxBuf(USCI_B_0, byte);
}

uint8_t usci_i2c_slave_receiveByte_blocking(usci_i2c_handler_t *i2c)
{
    return usci_readRxBuf(USCI_B_0);
}
