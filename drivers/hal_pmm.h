/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Power ManageMent Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_PMM_H
#define HAL_PMM_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"


typedef struct
{
    pmm_svsm_t side;
    pmm_enables_t enableSVS;
    pmm_enables_t enableSVM;
    pmm_overvoltageCtl_t overvoltageDetection;
    pmm_autoCtl_t automaticControl;
    pmm_lowPowerModesCtl_t lowPowerModesControl;
    pmm_perfomanceModes_t performanceSVS;
    pmm_perfomanceModes_t performanceSVM;
    pmm_eventMasks_t eventMask;
    pmm_eventMasks_t delayedEventMask;
} pmm_svsm_handler_t;


bool pmm_setSVSM(pmm_svsm_handler_t *svsmConfig);

bool pmm_setVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, pmm_voltageLevels_t newLevel);
bool pmm_setLowerVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, uint8_t newLevel);
bool pmm_setHigherVCore(pmm_svsm_handler_t *highside, pmm_svsm_handler_t *lowside, uint8_t newLevel);



static inline void pmm_highsideSVS_enable(void)
{
    SET_BIT(SVSMHCTL, SVSHE);
}

static inline void pmm_highsideSVM_enable(void)
{
    SET_BIT(SVSMHCTL, SVMHE);
}

static inline void pmm_highsideSVS_disable(void)
{
    CLEAR_BIT(SVSMHCTL, SVSHE);
}

static inline void pmm_highsideSVM_disable(void)
{
    CLEAR_BIT(SVSMHCTL, SVMHE);
}

static inline void pmm_lowsideSVS_enable(void)
{
    SET_BIT(SVSMLCTL, SVSLE);
}

static inline void pmm_lowsideSVM_enable(void)
{
    SET_BIT(SVSMLCTL, SVMLE);
}

static inline void pmm_lowsideSVS_disable(void)
{
    CLEAR_BIT(SVSMLCTL, SVSLE);
}

static inline void pmm_lowsideSVM_disable(void)
{
    CLEAR_BIT(SVSMLCTL, SVMLE);
}

static inline void pmm_unlockRegisters(void)
{
//    WRITE_REG(PMMCTL0_H, PMMPW_H);
    PMMCTL0_H = PMMPW_H;
}

static inline void pmm_lockRegisters(void)
{
    WRITE_REG(PMMCTL0_H, 0x00);
}


static inline uint8_t pmm_readVCore(void)
{
    return READ_BIT(PMMCTL0_L, PMMCOREV1_L | PMMCOREV0_L);
}


static inline void pmm_disableRegulator(void)
{
    SET_BIT(PMMCTL0, PMMREGOFF);
}

static inline void pmm_generatePOR(void)
{
    SET_BIT(PMMCTL0, PMMSWPOR);
}

static inline void pmm_generateBOR(void)
{
    SET_BIT(PMMCTL0, PMMSWBOR);
}

static inline void pmm_highPowerRequests_enable(void)
{
    SET_BIT(PMMCTL0_L, PMMHPMRE_L);
}

static inline void pmm_highPowerRequests_disable(void)
{
    CLEAR_BIT(PMMCTL0_L, PMMHPMRE_L);
}


static inline void pmm_setSVSresetVLevel(pmm_svsm_t svsm, pmm_SVSresetLevels_t lvl)
{
    SET_BIT((HWREG16(PMM_BASE + svsm)), lvl);
}

static inline uint8_t pmm_readSVSresetVLevel(pmm_svsm_t svsm)
{
    return READ_BIT(HWREG16(PMM_BASE + svsm), SVSHRVL0 | SVSHRVL1);
}

static inline void pmm_setSVSMresetReleaseVLevel(pmm_svsm_t svsm, pmm_SVSMresetReleaseLevels_t lvl)
{
    SET_BIT((HWREG16(PMM_BASE + svsm)), lvl);
}

static inline uint8_t pmm_readSVSMresetReleaseVLevel(pmm_svsm_t svsm)
{
    return READ_BIT(HWREG16(PMM_BASE + svsm), SVSMHRRL0 | SVSMHRRL1 | SVSMHRRL2);
}

static inline uint8_t pmm_readCoreVLevel()
{
    return READ_BIT(PMMCTL0, PMMCOREV0 | PMMCOREV1);
}


static inline void pmm_setSVMOUT(pmm_svmout_t svmout_setting, bool polarity)
{
    WRITE_REG(SVSMIO, svmout_setting);

    if (polarity)
        SET_BIT(SVSMIO, SVMOUTPOL);
}


static inline bool pmm_isInterruptPending(pmm_interrupts_t flag)
{
    return READ_BIT(PMMIFG, flag);
}

static inline void pmm_enableInterrupts(pmm_interrupts_t interrupts)
{
    SET_BIT(PMMRIE, interrupts);
}

static inline void pmm_disableInterrupts(pmm_interrupts_t interrupts)
{
    CLEAR_BIT(PMMRIE, ~interrupts);
}


#endif //HAL_PMM_H
