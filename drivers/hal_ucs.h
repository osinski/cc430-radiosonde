/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Unified Clock System Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_UCS_H
#define HAL_UCS_H

#include <cc430f5137.h>
#include <stdint.h>
#include <stdbool.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"



// only features needed for radiosonde are implemented - there's more
typedef struct
{
    ucs_xt1DriveModes_t xt1_driveMode;
    ucs_xt1CapacitorSelections_t xt1_internalCapacitor;

} ucs_oscillatorSettings_t;

void ucs_setOscillatorConfiguration(ucs_oscillatorSettings_t *oscSettings);

typedef struct
{
    ucs_dcoFrequencyRanges_t dco_frequencyRange;

    ucs_clkDividers_t fll_feedbackDivider;
    uint16_t fll_DCOmultiplier;
    ucs_fllReferences_t fll_referenceSource;
    ucs_fllClkDividers_t fll_referenceDivider;

} ucs_fllSettings_t;

void ucs_setFllConfiguration(ucs_fllSettings_t *fllSettings);

typedef struct
{
    ucs_clockType_t clock;
    ucs_clkSources_t clkSource;
    ucs_clkDividers_t clkSourceDivider;

} ucs_clockSettings_t;

void ucs_clockEnable(ucs_clockSettings_t *clkSettings);

ucs_clockSettings_t ucs_newDefaultClock(ucs_clockType_t clockType);

static inline void ucs_setAclkExternalPinSourceDivider(ucs_aclkExternalPinSourceDividers_t div)
{
    MODIFY_REG(UCSCTL5, __UCS_ACLK_EXTERNALPIN_SOURCE_DIVIDER_BITMASK, div);
}

static inline void ucs_setXT1alwaysOn(void)
{

}

static inline void ucs_setXT1notAlwaysOn(void)
{

}

static inline void ucs_setXT2alwaysOn(void)
{

}

static inline void ucs_setXT2notAlwaysOn(void)
{

}

static inline void ucs_enableClockRequests(ucs_clockRequests_t request)
{
    SET_BIT(UCSCTL8, request);
}

static inline void ucs_disableClockRequests(ucs_clockRequests_t request)
{
    CLEAR_BIT(UCSCTL8, request);
}

static inline void ucs_clearFaultFlag(ucs_faultFlags_t flag)
{
    CLEAR_BIT(UCSCTL7, flag);
}

static inline uint8_t ucs_readFaultFlags(void)
{
    return READ_REG(UCSCTL7_L);
}


#endif //HAL_UCS_H
