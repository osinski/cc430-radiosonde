/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 WatchDogTimer_A Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef WDT_A_H
#define WDT_A_H

#include <cc430f5137.h>
#include <stdint.h>

#include "rysio_defs.h"



static inline void wdt_holdWatchdog(void)
{
    WRITE_REG(WDTCTL, WDTPW + WDTHOLD);
}

static inline void wdt_clearWatchdog(void)
{
    // TODO
    WRITE_REG(WDTCTL, WDTPW + WDTCNTCL);
}


#endif
