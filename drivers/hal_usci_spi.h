/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Universal Serial Control Interface A Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef USCI_SPI_H
#define USCI_SPI_H

#include <cc430f5137.h>
#include <stdint.h>

#include "hal_common_defs.h"

typedef struct
{
    usci_peripherals_t peripheral;

    usci_uart_spi_dataDirection_t dataDirection;
    usci_uart_spi_characterLenght_t characterLen;

    usci_spi_mode_t spiMode;
    usci_spi_CSpinModes_t csPinMode;
    usci_spi_clockPhase_t clkPhase;
    usci_spi_clockPolarity_t clkPolarity;

} usci_spi_handler_t;


void usci_spi_init(usci_spi_handler_t *uart);

void usci_spi_transmitByte_blocking(usci_spi_handler_t *spi, uint8_t byte);
uint8_t usci_spi_receiveByte_blocking(usci_spi_handler_t *spi);


static inline
bool usci_spi_checkStatusFlag(usci_peripherals_t periph, usci_spi_statusFlags_t sflag)
{
    return (bool) READ_BIT(HWREG8(periph + OFFSET_USCI_STATUS), sflag);
}

#endif
