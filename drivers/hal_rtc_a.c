#include "hal_rtc_a.h"



#define RTC_CLOCK_OFFSET                        2
#define RTC_CLOCK_MASK                          3

#define RTC_CLOCK_PRESCALER_0_OFFSET            14
#define RTC_CLOCK_PRESCALER_0_MASK              1

#define RTC_CLOCK_PRESCALER_1_OFFSET            14
#define RTC_CLOCK_PRESCALER_1_MASK              3

#define RTC_CLOCK_PRESCALER_OFFSET              14
#define RTC_CLOCK_PRESCALER_DIVIDER_OFFSET      11



void rtc_a_initPrescaler(rtc_prescalers_t instance,
                         rtc_clocks_t clkSource,
                         rtc_prescalerDividers_t divider)
{
    // bitfield is reserved, so im gonna try and ignore PRESCALER_x_MASK
    uint16_t tmp = (clkSource << RTC_CLOCK_PRESCALER_OFFSET) |
               (divider << RTC_CLOCK_PRESCALER_DIVIDER_OFFSET);

    MODIFY_REG(HWREG16(RTC_A_BASE + instance), 0xFE,
               (clkSource << RTC_CLOCK_PRESCALER_OFFSET) |
               (divider << RTC_CLOCK_PRESCALER_DIVIDER_OFFSET));
}

void rtc_a_initCounterMode(rtc_clocks_t clkSource)
{
    rtc_a_setMode(RTC_MODE_COUNTER);
    MODIFY_REG(RTCCTL1, RTCSSEL0 | RTCSSEL1,
               clkSource << RTC_CLOCK_OFFSET);
}

void rtc_a_initCalendarMode(rtc_a_calendarMode_t *calendarModeSettings)
{
    rtc_a_setMode(RTC_MODE_CALENDAR);
    rtc_a_setFormat(calendarModeSettings->format);

    WRITE_REG(RTCSEC, calendarModeSettings->calendar_seconds);
    WRITE_REG(RTCMIN, calendarModeSettings->calendar_minutes);
    WRITE_REG(RTCHOUR, calendarModeSettings->calendar_hours);
    WRITE_REG(RTCDOW, calendarModeSettings->calendar_dayOfWeek);
    WRITE_REG(RTCDAY, calendarModeSettings->calendar_dayOfMonth);
    WRITE_REG(RTCMON, calendarModeSettings->calendar_month);
    WRITE_REG(RTCYEAR, calendarModeSettings->calendar_year);
}

