#include "hal_usci_spi.h"
#include "hal_usci_common.h"


usci_spi_handler_t usci_ab_spi_handler = {
    .peripheral = USCI_A_0,

    .dataDirection = USCI_UART_SPI_DATADIRECTION_LSBFIRST,
    .characterLen = USCI_UART_SPI_CHARLEN_7BIT,

    .spiMode = USCI_SPI_MODE_SLAVE,
    .csPinMode = USCI_SPI_CS_SOFTWARE,
    .clkPhase = USCI_SPI_CLOCK_PHASE_CHANGE_CAPTURE,
    .clkPolarity = USCI_SPI_CLOCK_POLARITY_IDLELOW
};


void usci_spi_init(usci_spi_handler_t *spi)
{
    usci_uart_spi_setDataDirection(spi->peripheral, spi->dataDirection);
    usci_uart_spi_setCharacterLength(spi->peripheral, spi->characterLen);

    MODIFY_REG(HWREG8(spi->peripheral + OFFSET_USCI_CONTROL0), UCCKPH, spi->clkPhase);
    MODIFY_REG(HWREG8(spi->peripheral + OFFSET_USCI_CONTROL0), UCCKPL, spi->clkPolarity);
    MODIFY_REG(HWREG8(spi->peripheral + OFFSET_USCI_CONTROL0), UCMST, spi->spiMode);

    SET_BIT(HWREG8(spi->peripheral + OFFSET_USCI_CONTROL0), UCSYNC);
    MODIFY_REG(HWREG8(spi->peripheral + OFFSET_USCI_CONTROL0), __USCI_SPI_CS_BITMASK, spi->csPinMode);
}

void usci_spi_transmitByte_blocking(usci_spi_handler_t *spi, uint8_t byte)
{
    // tx interrupt flag is set when txbuf is empty - check for that
    while (!(usci_readInterruptFlags(spi->peripheral) & USCI_INTERRUPTS_TX));

    usci_writeTxBuf(spi->peripheral, byte);
}

uint8_t usci_spi_receiveByte_blocking(usci_spi_handler_t *spi)
{
    // rx interrupt flag is set when rxbuf is not empty - check for that
    while (!(usci_readInterruptFlags(spi->peripheral) & USCI_INTERRUPTS_RX));

    return usci_readRxBuf(spi->peripheral);
}
