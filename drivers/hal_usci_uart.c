#include "hal_usci_uart.h"
#include "hal_usci_common.h"


usci_uart_handler_t usci_a_uart_handler = {
    .dataDirection = USCI_UART_SPI_DATADIRECTION_LSBFIRST,
    .characterLen = USCI_UART_SPI_CHARLEN_8BIT,
    .mode = USCI_UART_MODE_UART,
    .parity = USCI_UART_PARITY_DISABLED,
    .stopBits = USCI_UART_STOPBITS_ONE,
    .baudrateMode = USCI_UART_BAUDRATE_GENERATOR_LOWFREQ
};

void usci_uart_init(usci_uart_handler_t *uart)
{
    usci_uart_spi_setDataDirection(USCI_A_0, uart->dataDirection);
    usci_uart_spi_setCharacterLength(USCI_A_0, uart->characterLen);

    MODIFY_REG(UCA0CTL0, UCSPB, uart->stopBits);
    MODIFY_REG(UCA0CTL0, __USCI_UART_PARITY_BITMASK, uart->parity);

    CLEAR_BIT(UCA0CTL0, UCSYNC);
    MODIFY_REG(UCA0CTL0, __USCI_UART_MODE_BITMASK, uart->mode);

    MODIFY_REG(UCA0MCTL, UCOS16, uart->baudrateMode);
}

void usci_uart_transmitByte_blocking(uint8_t byte)
{
    // tx interrupt flag is set when txbuf is empty - check for that
    while (!(usci_readInterruptFlags(USCI_A_0) & USCI_INTERRUPTS_TX));

    usci_writeTxBuf(USCI_A_0, byte);
}

uint8_t usci_uart_receiveByte_blocking(void)
{
    // rx interrupt flag is set when rxbuf is not empty - check for that
    while (!(usci_readInterruptFlags(USCI_A_0) & USCI_INTERRUPTS_RX));

    return usci_readRxBuf(USCI_A_0);
}

void usci_uart_transmitAddress(uint8_t addr)
{
    usci_uart_markNextByteAsAddress();

    usci_uart_transmitByte_blocking(addr);
}

void usci_uart_transmitBreak(void)
{
    usci_uart_markNextByteAsBreak();

    usci_uart_transmitByte_blocking(0x55);
}
