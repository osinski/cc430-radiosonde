/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Real Time Clock Module Driver (RTC_A specific)
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_RTC_A_H
#define HAL_RTC_A_H

#include <cc430f5137.h>
#include <stdint.h>

#include "rysio_defs.h"
#include "hal_common_defs.h"



void rtc_a_initPrescaler(rtc_prescalers_t instance,
                         rtc_clocks_t clkSource,
                         rtc_prescalerDividers_t divider);

void rtc_a_initCounterMode(rtc_clocks_t clkSource);

typedef struct
{
    rtc_formats_t format;
    uint8_t calendar_seconds;
    uint8_t calendar_minutes;
    uint8_t calendar_hours;
    uint8_t calendar_dayOfWeek;
    uint8_t calendar_dayOfMonth;
    uint8_t calendar_month;
    uint16_t calendar_year;
} rtc_a_calendarMode_t;

void rtc_a_initCalendarMode(rtc_a_calendarMode_t *calendarModeSettings);


#define ALARM_ENABLE    (1<<7)
static inline
void rtc_a_setAlarm(rtc_alarmRegs_t alarm, uint8_t value)
{
    WRITE_REG(HWREG8(RTC_A_BASE + alarm), value | ALARM_ENABLE);
}

static inline
void rtc_a_disableAlarm(rtc_alarmRegs_t alarm)
{
    CLEAR_BIT(HWREG8(RTC_A_BASE + alarm), ALARM_ENABLE);
}
#undef ALARM_ENABLE

static inline
void rtc_a_setHold_prescaler(rtc_prescalers_t prescaler)
{
    SET_BIT(HWREG16(RTC_A_BASE + prescaler), RT0PSHOLD);
}

static inline
void rtc_a_setHold_mainCounter(void)
{
    SET_BIT(RTCCTL1, RTCHOLD);
}

static inline
void rtc_a_clearHold_prescaler(rtc_prescalers_t prescaler)
{
    CLEAR_BIT(HWREG16(RTC_A_BASE + prescaler), RT0PSHOLD);
}

static inline
void rtc_a_clearHold_mainCounter(void)
{
    CLEAR_BIT(RTCCTL1, RTCHOLD_H);
}

static inline
void rtc_a_setMode(rtc_modes_t mode)
{
    MODIFY_REG(RTCCTL1, RTCMODE, mode);
}

static inline
void rtc_a_setFormat(rtc_formats_t format)
{
    MODIFY_REG(RTCCTL1, RTCBCD, format);
}

static inline
bool rtc_a_isSafeToRead(void)
{
    return (bool) READ_BIT(RTCCTL1, RTCRDY);
}

static inline
void rtc_a_setTimeEventOnOverflow(rtc_overflows_t event)
{
    MODIFY_REG(RTCCTL1, RTCTEV0 | RTCTEV1, event);
}

static inline
void rtc_a_setCalibrationClock(rtc_calibrationClocks_t calClock)
{
    WRITE_REG(RTCCTL3, calClock);
}

static inline
void rtc_a_setCalibrationOffset(rtc_calibrationLSB_t lsb, uint8_t offset)
{
    WRITE_REG(RTCCTL2, (offset & 0x1f) | lsb);
}

static inline
void rtc_a_writeCounterReg(rtc_counters_t cnt, uint8_t value)
{
    WRITE_REG(HWREG8(RTC_A_BASE + cnt), value);
}

static inline
uint8_t rtc_a_readCounterReg(rtc_counters_t cnt)
{
    return READ_REG(HWREG8(RTC_A_BASE + cnt));
}

static inline
void rtc_a_writeCounterRegs(uint32_t val)
{
    WRITE_REG(HWREG32(RTCNT1), val);
}

static inline
uint32_t rtc_a_readCounterRegs(void)
{
    return READ_REG(HWREG32(RTCNT1));
}

static inline
void rtc_a_enableInterrupt(rtc_interrupts_t interrupt)
{
    SET_BIT(RTCCTL0, interrupt << 4);
}

static inline
void rtc_a_disableInterrupt(rtc_interrupts_t interrupt)
{
    CLEAR_BIT(RTCCTL0, interrupt << 4);
}

static inline
void rtc_a_clearInterruptFlag(rtc_interrupts_t interrupt)
{
    CLEAR_BIT(RTCCTL0, interrupt);
}

static inline
uint8_t rtc_a_readInterruptFlags(void)
{
    return READ_REG(RTCCTL0) & 7;
}

static inline
void rtc_a_enablePrescalerInterruptWithInterval(rtc_prescalers_t prescaler,
                                                rtc_prescalerDividers_t interval)
{
    MODIFY_REG(HWREG16(RTC_A_BASE + prescaler), RT0IP0|RT0IP1|RT0IP2|RT0PSIE,
                                                (interval << 2) | RT0PSIE);
}

static inline
void rtc_a_disablePrescalerInterrupt(rtc_prescalers_t prescaler)
{
    CLEAR_BIT(HWREG16(RTC_A_BASE + prescaler), RT0PSIE);
}

static inline
void rtc_a_clearPrescalerInterruptFlag(rtc_prescalers_t prescaler)
{
    CLEAR_BIT(HWREG16(RTC_A_BASE + prescaler), RT0PSIFG);
}

static inline
bool rtc_a_readPrescalerInterruptFlag(rtc_prescalers_t prescaler)
{
    return (bool) READ_BIT(HWREG16(RTC_A_BASE + prescaler), RT0PSIFG);
}


#endif //HAL_RTC_A_H
