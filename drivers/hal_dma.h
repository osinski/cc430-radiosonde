/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 DMA Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef HAL_DMA_H
#define HAL_DMA_H

#include <cc430f5137.h>
#include <stdint.h>

#endif //HAL_DMA_H
