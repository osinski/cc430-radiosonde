/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Universal Serial Control Interface B Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef MPY32_H
#define MPY32_H

#include <cc430f5137.h>
#include <stdint.h>

#include "hal_common_defs.h"

//typedef struct
//{

//} mpy32_handler_t;

#endif
