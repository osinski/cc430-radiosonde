/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Universal Serial Control Interface B Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef USCI_UART_H
#define USCI_UART_H

#include <cc430f5137.h>
#include <stdint.h>

#include "hal_common_defs.h"


typedef struct
{
    usci_uart_spi_dataDirection_t dataDirection;
    usci_uart_spi_characterLenght_t characterLen;

    usci_uart_modes_t mode;
    usci_uart_parity_t parity;
    usci_uart_stopBitsCount_t stopBits;
    usci_uart_baudrateGenerationModes_t baudrateMode;

} usci_uart_handler_t;

void usci_uart_init(usci_uart_handler_t *uart);

void usci_uart_transmitByte_blocking(uint8_t byte);
uint8_t usci_uart_receiveByte_blocking(void);
void usci_uart_transmitAddress(uint8_t addr);
void usci_uart_transmitBreak(void);


static inline
void usci_uart_setFirstModulationStage(uint8_t val)
{
    MODIFY_REG(UCA0MCTL, UCBRF0 | UCBRF1 | UCBRF2 | UCBRF3, (val & 16) << 4);
}

static inline
void usci_uart_setSecondModulationStage(uint8_t val)
{
    MODIFY_REG(UCA0MCTL, UCBRS0 | UCBRS1 | UCBRS2, (val & 8) << 1);
}

static inline
void usci_uart_setModulationStages(uint8_t first, uint8_t second)
{
    usci_uart_setFirstModulationStage(first);
    usci_uart_setSecondModulationStage(second);
}

static inline
void usci_uart_enableSpecialInterrupt(usci_uart_specialInterrupts_t interrupt)
{
    SET_BIT(UCA0CTL1, interrupt);
}

static inline
void usci_uart_disableSpecialInterrupt(usci_uart_specialInterrupts_t interrupt)
{
    CLEAR_BIT(UCA0CTL1, interrupt);
}

static inline
bool usci_uart_checkStatusFlag(usci_uart_statusFlags_t sflag)
{
    return READ_BIT(UCA0STAT, sflag);
}

static inline
void usci_uart_setDormantMode(void)
{
    SET_BIT(UCA0CTL1, UCDORM);
}

static inline
void usci_uart_resetDormantMode(void)
{
    CLEAR_BIT(UCA0CTL1, UCDORM);
}

static inline
void usci_uart_markNextByteAsAddress(void)
{
    SET_BIT(UCA0CTL1, UCADDR);
}

static inline
void usci_uart_markNextByteAsBreak(void)
{
    SET_BIT(UCA0CTL1, UCBRK);
}


#endif
