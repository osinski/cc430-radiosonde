cmake_minimum_required(VERSION 3.13)

add_library(drivers OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/hal_dma.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_gpio.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_mpy32.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_pmm.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_rf1a.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_rtc_a.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_sys.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_timer_a.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_ucs.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_usci_common.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_usci_i2c.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_usci_spi.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_usci_uart.c
    ${CMAKE_CURRENT_LIST_DIR}/hal_wdt_a.c
)

target_include_directories(drivers
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}
)
