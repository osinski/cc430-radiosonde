/*******************************************************************************
 *
 * Microsystems Oriented Society
 *
 * CC430F6137 Universal Serial Control Interface B Module Driver
 *
 * Part of the Radiosonde Software
 * Used in High Altitude Balloon
 *
 * Marcin Osiński 2019
 *
 * ****************************************************************************/

#ifndef USCI_COMMON_H
#define USCI_COMMON_H

#include <cc430f5137.h>
#include <stdint.h>

#include "hal_common_defs.h"

typedef union
{
    uint8_t regsBR[2];
    uint16_t baudrate;
} usci_baudrateReg_t;

typedef struct
{
    usci_clocks_t clock;
    usci_baudrateReg_t baudrate_prescalers;
} usci_clock_handler_t;

void usci_clock_init(usci_peripherals_t periph, usci_clock_handler_t *clk);


static inline
void usci_setResetState(usci_peripherals_t periph)
{
    SET_BIT(HWREG8(periph + OFFSET_USCI_CONTROL1), UCSWRST);
}

static inline
void usci_clearResetState(usci_peripherals_t periph)
{
    CLEAR_BIT(HWREG8(periph + OFFSET_USCI_CONTROL1), UCSWRST);
}


static inline
void usci_writeTxBuf(usci_peripherals_t periph, uint8_t byte)
{
    WRITE_REG(HWREG8(periph + OFFSET_USCI_TXBUF), byte);
}

static inline
uint8_t usci_readRxBuf(usci_peripherals_t periph)
{
    return READ_REG(HWREG8(periph + OFFSET_USCI_RXBUF));
}

static inline
void usci_writeBaudRateSetting(usci_peripherals_t periph, usci_baudrateReg_t br)
{
    WRITE_REG(HWREG16(periph + OFFSET_USCI_BAUDRATE0), br.baudrate);
}

static inline
void usci_setClock(usci_peripherals_t periph, usci_clocks_t clock)
{
    MODIFY_REG(HWREG8(periph + OFFSET_USCI_CONTROL1), __USCI_CLOCK_BITMASK, clock);
}

static inline
void usci_uart_spi_enableListenMode(usci_peripherals_t periph)
{
    SET_BIT(HWREG8(periph + OFFSET_USCI_STATUS), UCLISTEN);
}

static inline
void usci_uart_spi_disableListenMode(usci_peripherals_t periph)
{
    CLEAR_BIT(HWREG8(periph + OFFSET_USCI_STATUS), UCLISTEN);
}

static inline
void usci_uart_spi_setDataDirection(usci_peripherals_t periph, usci_uart_spi_dataDirection_t dataDir)
{
    MODIFY_REG(HWREG8(periph + OFFSET_USCI_CONTROL0), UCMSB, dataDir);
}

static inline
void usci_uart_spi_setCharacterLength(usci_peripherals_t periph, usci_uart_spi_characterLenght_t charLen)
{
    MODIFY_REG(HWREG8(periph + OFFSET_USCI_CONTROL0), UC7BIT, charLen);
}

static inline
void usci_enableInterrupt(usci_peripherals_t periph, usci_interrupts_t interrupt)
{
    SET_BIT(HWREG8(periph + OFFSET_USCI_INTERRUPTS_ENABLE), interrupt);
}

static inline
void usci_disableInterrupt(usci_peripherals_t periph, usci_interrupts_t interrupt)
{
    CLEAR_BIT(HWREG8(periph + OFFSET_USCI_INTERRUPTS_ENABLE), interrupt);
}

static inline
void usci_clearInterruptFlag(usci_peripherals_t periph, usci_interrupts_t interrupt)
{
    CLEAR_BIT(HWREG8(periph + OFFSET_USCI_INTERRUPTS_FLAGS), interrupt);
}

static inline
uint8_t usci_readInterruptFlags(usci_peripherals_t periph)
{
    return READ_REG(HWREG8(periph + OFFSET_USCI_INTERRUPTS_FLAGS));
}

static inline
uint8_t usci_readStatusRegister(usci_peripherals_t periph)
{
    return READ_REG(HWREG8(periph + OFFSET_USCI_STATUS));
}

static inline
bool usci_isBusy(usci_peripherals_t periph)
{
    return (bool) READ_BIT(HWREG8(periph + OFFSET_USCI_STATUS), UCBUSY);
}


#endif /* USCI_COMMON_H */
